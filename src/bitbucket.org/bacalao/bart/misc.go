package bart

// import (
// 	cfg "bitbucket.org/bacalao/bart/config"
// 	"bitbucket.org/bacalao/bart/logger"
// 	"bitbucket.org/bacalao/bart/storage"
// 	"bitbucket.org/bacalao/bart/utils"
// 	mgo "gopkg.in/mgo.v2"
// 	"gopkg.in/mgo.v2/bson"
// 	"time"
// )

// func (app App) initAccounts(session *mgo.Session) error {
// 	accounts := session.DB(cfg.C.MongoDB.Database).C("accounts")
// 	// Create unique index for name field.
// 	if err := accounts.EnsureIndex(mgo.Index{
// 		Key:    []string{"name"},
// 		Unique: true,
// 		Name:   "accounts_name_unique",
// 	}); err != nil {
// 		return err
// 	}
// 	// Make sure that sniperlead account exists.
// 	count, err := accounts.Find(bson.M{"name": "sniperlead"}).Count()
// 	if err != nil {
// 		return err
// 	}
// 	if count == 0 {
// 		logger.L.Debug("sniperlead account does not exists")
// 		if err = accounts.Insert(&storage.Account{
// 			Id:        storage.BACALAO_ACCOUNT_ID,
// 			Name:      "sniperlead",
// 			Deleted:   false,
// 			Status:    storage.ACTIVE,
// 			CreatedAt: time.Now(),
// 			UpdatedAt: time.Now(),
// 		}); err != nil {
// 			return err
// 		}
// 		logger.L.Debug("sniperlead account was created")
// 	}
// 	return nil
// }

// func (app App) initUsers(session *mgo.Session) error {
// 	users := session.DB(cfg.C.MongoDB.Database).C("users")
// 	// Create unique index for email field.
// 	if err := users.EnsureIndex(mgo.Index{
// 		Key:    []string{"email"},
// 		Unique: true,
// 		Name:   "users_email_unique",
// 	}); err != nil {
// 		return err
// 	}
// 	// Make sure that robot user exists.
// 	count, err := users.Find(bson.M{"email": "robot@sniperlead.com"}).Count()
// 	if err != nil {
// 		return err
// 	}
// 	if count == 0 {
// 		logger.L.Debug("robot user does not exists")
// 		if err = users.Insert(&storage.User{
// 			Id:       storage.ROBOT_USER_ID,
// 			Deleted:  false,
// 			Status:   storage.ACTIVE,
// 			Verified: true,
// 			AccountRef: mgo.DBRef{
// 				Collection: "accounts",
// 				Id:         storage.BACALAO_ACCOUNT_ID,
// 			},
// 			Type:      storage.ROBOT,
// 			Email:     "robot@sniperlead.com",
// 			Password:  utils.NO_PASSWORD,
// 			CreatedAt: time.Now(),
// 			UpdatedAt: time.Now(),
// 		}); err != nil {
// 			return err
// 		}
// 		logger.L.Debug("robot user was created")
// 	}
// 	return nil
// }

// func (app App) initDomens(session *mgo.Session) error {
// 	domens := session.DB(cfg.C.MongoDB.Database).C("domens")
// 	if err := domens.EnsureIndex(mgo.Index{
// 		Key:    []string{"host"},
// 		Unique: true,
// 		Name:   "domens_host_unique",
// 	}); err != nil {
// 		return err
// 	}
// 	// Make sure that sniperlead.com domen exists.
// 	domen := storage.Domen{}
// 	err := domens.Find(bson.M{"host": "sniperlead.com"}).One(&domen)
// 	if err != nil {
// 		if err != mgo.ErrNotFound {
// 			return err
// 		}
// 		logger.L.Debug("sniperlead.com domen does not exists")
// 		if err := domens.Insert(&storage.Domen{
// 			AccountRef: mgo.DBRef{
// 				Collection: "accounts",
// 				Id:         storage.BACALAO_ACCOUNT_ID,
// 			},
// 			Rules: storage.Rules{
// 				TimeSpentOnPage: storage.TimeSpentOnPageRule{
// 					Points:        100,
// 					SpentMoreThen: 5,
// 				},
// 			},
// 			Host:      "sniperlead.com",
// 			Status:    storage.ACTIVE,
// 			Deleted:   false,
// 			CreatedAt: time.Now(),
// 			UpdatedAt: time.Now(),
// 		}); err != nil {
// 			return err
// 		}
// 		logger.L.Debug("sniperlead.com domen was created")
// 	}
// 	return nil
// }

// func (app App) initStorage() error {
// 	var err error

// 	logger.L.Debug("Starting storage initialization")
// 	session, err := storage.GetSession()
// 	if err != nil {
// 		return err
// 	}
// 	defer session.Close()

// 	if err = app.initAccounts(session); err != nil {
// 		return err
// 	}
// 	if err = app.initUsers(session); err != nil {
// 		return err
// 	}
// 	if err = app.initDomens(session); err != nil {
// 		return err
// 	}

// 	logger.L.Debug("Storage initialization completed")
// 	return nil
// }
