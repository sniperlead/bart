package logger

import "github.com/op/go-logging"

var L *logging.Logger

func init() {
	f := "%{color}%{time:15:04:05.000} %{longfunc} ▶ %{level:.4s} %{id:03x} %{message}%{color:reset}"
	logging.SetFormatter(logging.MustStringFormatter(f))
	L = logging.MustGetLogger("bacalao")
}
