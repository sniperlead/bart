package utils

import "time"

func JSTimeToTime(t int64) time.Time {
	seconds := int64(float64(t) / 1000.0)
	milliseconds := t - seconds*1000
	return time.Unix(seconds, milliseconds*1000000)
}

func ParseVoxImplantDate(d string) (time.Time, error) {
	if t, err := time.Parse("2006-01-02 15:04:05", d); err == nil {
		return t.UTC(), nil
	} else {
		return t, err
	}
}
