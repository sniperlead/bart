package utils

import (
	"crypto/md5"
	"crypto/sha256"
	"fmt"
	"hash"
	"io"
	"strings"
)

const NO_PASSWORD = "__no_password__"

func encodePassword(hash hash.Hash, algorithm, password, salt string) string {
	io.WriteString(hash, fmt.Sprintf("%s.%s", password, salt))
	return fmt.Sprintf("%s$%s$%x", algorithm, salt, hash.Sum(nil))
}

func verifyPassword(hash hash.Hash, algorithm, password, encoded string) bool {
	if encoded == NO_PASSWORD {
		return false
	}
	parts := strings.Split(encoded, "$")
	if len(parts) < 3 {
		return false
	}
	return encodePassword(hash, algorithm, password, parts[1]) == encoded
}

type Hasher interface {
	Encode(password, salt string) string
	Verify(password, encoded string) bool
}

type md5Hasher struct{}

var MD5Hasher md5Hasher = md5Hasher{}

func (hasher md5Hasher) Encode(password, salt string) string {
	return encodePassword(md5.New(), "md5", password, salt)
}

func (hasher md5Hasher) Verify(password, encoded string) bool {
	return verifyPassword(md5.New(), "md5", password, encoded)
}

type sha256Hasher struct{}

var SHA256Hasher sha256Hasher = sha256Hasher{}

func (hasher sha256Hasher) Encode(password, salt string) string {
	return encodePassword(sha256.New(), "sha256", password, salt)
}

func (hasher sha256Hasher) Verify(password, encoded string) bool {
	return verifyPassword(sha256.New(), "sha256", password, encoded)
}

func GetHasher(encoded string) Hasher {
	parts := strings.Split(encoded, ".")
	switch parts[0] {
	case "md5":
		return &MD5Hasher
	case "sha256":
		return &SHA256Hasher
	}
	return nil
}
