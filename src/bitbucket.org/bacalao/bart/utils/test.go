package utils

import (
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/dbtest"
	"io/ioutil"
	"os"
)

type TestEnv struct {
	// Path to test mongodb.
	dbPath string
	// Test mongodb server.
	dbServer *dbtest.DBServer
	// Test mongodb session.
	Session *mgo.Session
	// Test mongodb database.
	DB *mgo.Database
}

func (e TestEnv) Clean() {
	e.Session.Close()
	e.dbServer.Stop()
	os.RemoveAll(e.dbPath)
}

func SetupTestEnv() (*TestEnv, error) {
	e := TestEnv{}
	var err error

	if e.dbPath, err = ioutil.TempDir("", "mongodb-"); err != nil {
		return nil, err
	}

	e.dbServer = &dbtest.DBServer{}
	e.dbServer.SetPath(e.dbPath)

	e.Session = e.dbServer.Session()

	e.DB = e.Session.DB("")

	return &e, nil
}
