package utils

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_MD5Hasher_Encode(t *testing.T) {
	assert := assert.New(t)

	encoded := MD5Hasher.Encode("password", "123")
	assert.Equal(encoded, "md5$123$15b421d36a6cc2028ff1d0f8f29c65ab")
}

func Test_MD5Hasher_Verify(t *testing.T) {
	assert := assert.New(t)

	encoded := MD5Hasher.Encode("password", "123")
	assert.True(MD5Hasher.Verify("password", encoded))
	assert.False(MD5Hasher.Verify("password1", encoded))
}

func Test_SHA256Hasher_Encode(t *testing.T) {
	assert := assert.New(t)

	encoded := SHA256Hasher.Encode("password", "123")
	assert.Equal(encoded, "sha256$123$90dd9a873ed29902c543fe5cbb0a01268e7a7adadfc91bb135e800e1260f5cb2")
}

func Test_SHA256Hasher_Verify(t *testing.T) {
	assert := assert.New(t)

	encoded := SHA256Hasher.Encode("password", "123")
	assert.True(SHA256Hasher.Verify("password", encoded))
	assert.False(SHA256Hasher.Verify("password1", encoded))
}
