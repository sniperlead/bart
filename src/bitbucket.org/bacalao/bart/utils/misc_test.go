package utils

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func Test_JSTimeToTime(t *testing.T) {
	assert := assert.New(t)

	// js time number of milliseconds since 01/01/1970 00:00:00
	var jsTime int64 = 1437091933200 // Thu Jul 16 2015 17:12:13 GMT-0700 (PDT)
	r := JSTimeToTime(jsTime)

	assert.Equal(r.Day(), 16)
	assert.Equal(r.Month(), time.July)
	assert.Equal(r.Year(), 2015)
	assert.Equal(r.Hour(), 17)
	assert.Equal(r.Minute(), 12)
	assert.Equal(r.Second(), 13)
}

func Test_ParseVoxImplantDate(t *testing.T) {
	assert := assert.New(t)

	r, err := ParseVoxImplantDate("2015-01-15 12:13:14")
	assert.Nil(err)
	assert.Equal(r.Year(), 2015)
	assert.Equal(r.Month(), time.January)
	assert.Equal(r.Day(), 15)
	assert.Equal(r.Hour(), 12)
	assert.Equal(r.Minute(), 13)
	assert.Equal(r.Second(), 14)
}
