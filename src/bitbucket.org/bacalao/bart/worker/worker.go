package worker

type Worker interface {
	Start()
	ShutDown()
}
