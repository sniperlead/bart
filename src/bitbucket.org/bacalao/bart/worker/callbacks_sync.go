package worker

import (
	"bitbucket.org/bacalao/bart/logger"
	"bitbucket.org/bacalao/bart/service"
	"time"
)

type SyncCallbacksWorker interface {
	Worker
}

type SyncCallbacksWorkerImpl struct {
	shutingDown bool
	sessionSvc  service.SessionService
	callbackSvc service.CallbackService
	updateCbWrk UpdateCallbackWorker
}

func (w *SyncCallbacksWorkerImpl) ShutDown() {
	w.shutingDown = true
}

func (w SyncCallbacksWorkerImpl) syncCallbacks() {
	s, err := w.sessionSvc.GetSession()
	if err != nil {
		logger.L.Error("Cannot connect to mongodb: %s", err)
		return
	}
	defer s.Close()
	db := s.DB("")

	for true {
		callbacks, err := w.callbackSvc.FindUnsynced(db, 10)
		if err != nil {
			logger.L.Error("Cannot get a list of unsynced callbacks: %s", err)
			return
		}
		if len(callbacks) == 0 {
			break
		}
		for _, cb := range callbacks {
			logger.L.Debug("Syncing callback {id: %s}", cb.Id.Hex())
			w.updateCbWrk.UpdateCallbackSync(&cb.Id)
		}
	}
}

func (w *SyncCallbacksWorkerImpl) Start() {
	logger.L.Debug("Starting sync callbacks worker")
	go func() {
		for !w.shutingDown {
			w.syncCallbacks()
			time.Sleep(5 * time.Second)
		}
	}()
}

func NewSyncCallbacksWorker(
	session service.SessionService,
	callback service.CallbackService,
	updateCbWrk UpdateCallbackWorker,
) SyncCallbacksWorker {
	return &SyncCallbacksWorkerImpl{
		shutingDown: false,
		sessionSvc:  session,
		callbackSvc: callback,
		updateCbWrk: updateCbWrk,
	}
}
