package worker

import (
	"bitbucket.org/bacalao/bart/errors"
	"bitbucket.org/bacalao/bart/logger"
	"bitbucket.org/bacalao/bart/service"
	"bitbucket.org/bacalao/bart/utils"
	"bitbucket.org/bacalao/bart/voximplant"
	"gopkg.in/mgo.v2/bson"
	"strconv"
)

type UpdateCallbackWorker interface {
	Worker
	UpdateCallback(id *bson.ObjectId)
	UpdateCallbackSync(id *bson.ObjectId) errors.Error
}

type CallbackUpdateWorkerImpl struct {
	shutingDown   bool
	ids           chan *bson.ObjectId
	sessionSvc    service.SessionService
	callbackSvc   service.CallbackService
	voxImplantSvc service.VoxImplantService
}

func (w *CallbackUpdateWorkerImpl) ShutDown() {
	w.shutingDown = true
	close(w.ids) // close channel
}

func (w *CallbackUpdateWorkerImpl) Start() {
	logger.L.Debug("Starting callback update worker")
	go func() {
		for id := range w.ids {
			if w.shutingDown {
				break
			}
			if id != nil {
				logger.L.Debug("Got callback %s", id.Hex())
				go w.UpdateCallbackSync(id)
			}
		}
	}()
}

func (w *CallbackUpdateWorkerImpl) UpdateCallback(id *bson.ObjectId) {
	w.ids <- id
}

func addCallInfoToChanges(prefix string, info *voximplant.CallInfo, c bson.M) {
	c[prefix+"id"] = strconv.Itoa(info.Id)
	c[prefix+"successful"] = info.Successful
	c[prefix+"phone_number_type"] = info.RemoteNumberType
	c[prefix+"transaction_id"] = strconv.Itoa(info.TransactionId)
	c[prefix+"duration"] = info.Duration
	if info.RecordURL != "" {
		c[prefix+"record_url"] = info.RecordURL
	}
	c[prefix+"cost"] = info.Cost
	if t, err := utils.ParseVoxImplantDate(info.StartTime); err != nil {
		c[prefix+"start_time"] = &t
	}
}

func (w *CallbackUpdateWorkerImpl) UpdateCallbackSync(id *bson.ObjectId) errors.Error {
	s, err := w.sessionSvc.GetSession()
	if err != nil {
		logger.L.Error("Cannot connect to mongodb: %s", err)
		return err
	}
	defer s.Close()
	db := s.DB("")

	cb, err := w.callbackSvc.GetById(db, *id)
	if err != nil {
		logger.L.Error("Cannot get callback {id: %s}: %s", id.Hex(), err)
		return err
	}

	h, err := w.voxImplantSvc.GetCallHistoryByCustomData(cb.ExternalId)
	if err != nil {
		logger.L.Error("Cannot get call history {data: %s}: %s", cb.Token, err)
		return err
	}
	if len(h.Result) != 0 {
		changes := bson.M{}
		changes["duration"] = h.Result[0].Duration
		if h.Result[0].LogFileUrl != "" {
			changes["log_file_url"] = h.Result[0].LogFileUrl
		}
		changes["session_id"] = strconv.Itoa(h.Result[0].CallSessionHistoryId)
		cost := 0.0
		if len(h.Result[0].Calls) != 0 {
			var accountCall *voximplant.CallInfo = nil
			var clientCall *voximplant.CallInfo = nil
			for _, c := range h.Result[0].Calls {
				if c.CustomData == "account_call" {
					accountCall = &c
				}
				if c.CustomData == "client_call" {
					clientCall = &c
				}
			}
			if accountCall != nil {
				addCallInfoToChanges("account_call.", accountCall, changes)
				cost += accountCall.Cost
			}
			if clientCall != nil {
				addCallInfoToChanges("client_call.", clientCall, changes)
				cost += clientCall.Cost
			}
		}
		changes["cost"] = cost
		if err := w.callbackSvc.Update(db, cb.Id, changes); err != nil {
			logger.L.Error("Cannot update callback {id: %s}: %s", id.Hex(), err)
			return err
		}
	}
	return nil
}

func NewUpdateCallbackWorker(
	session service.SessionService,
	callback service.CallbackService,
	voxImplant service.VoxImplantService,
) UpdateCallbackWorker {
	return &CallbackUpdateWorkerImpl{
		shutingDown:   false,
		ids:           make(chan *bson.ObjectId),
		sessionSvc:    session,
		callbackSvc:   callback,
		voxImplantSvc: voxImplant,
	}
}
