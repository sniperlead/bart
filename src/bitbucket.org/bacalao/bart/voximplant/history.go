package voximplant

import (
	"encoding/json"
	"net/url"
	"time"
)

type GetCallHistoryRequest struct {
	// The UTC from date filter in format YYYY-MM-DD HH:mm:SS.
	FromDate time.Time `required:"true" name:"from_date"`
	// The UTC to date filter in format YYYY-MM-DD HH:mm:SS.
	ToDate time.Time `required:"true" name:"to_date"`
	// The application ID.
	ApplicationId int `required:"false" name:"application_id"`
	// The application name.
	ApplicationName string `required:"false" name:"application_name"`
	// The call session history ID list separated by the ';' symbol.
	CallSessionHistoryId string `required:"false" name:"call_session_history_id"`
	// The user ID list to filter separated by the ';' symbol.
	UserId string `required:"false" name:"user_id"`
	// The rule name to filter.
	RuleName string `required:"false" name:"rule_name"`
	// The remote number to filter.
	RemoteNumber string `required:"false" name:"remote_number"`
	// The local number to filter.
	LocalNumber string `required:"false" name:"local_number"`
	// The custom_data to filter sessions.
	CallSessionHistoryCustomData string `required:"false" name:"call_session_history_custom_data"`
	// Set true to get binding calls.
	WithCalls bool `required:"false" name:"with_calls" default:"false"`
	// Set true to get binding records.
	WithRecords bool `required:"false" name:"with_records" default:"false"`
	// Set true to get other resources usage.
	WithOtherResources bool `required:"false" name:"with_other_resources" default:"false"`
	// The child account ID list separated by the ';' symbol or the 'all' value.
	ChileAccountId string `required:"false" name:"child_account_id"`
	// Set true to get the children account calls only.
	ChildrenCallsOnly bool `required:"false" name:"children_calls_only"`
	// Set false to get a CSV file without the column names if the output=csv.
	WithHeader bool `required:"false" name:"with_header"`
	// Set true to get records in the descent order.
	DescOrder bool `required:"false" name:"desc_order"`
	// The max returning record count.
	Count int `required:"false" name:"count" default:"20"`
	// The record count to omit.
	Offset int `required:"false" name:"offset"`
	// The output format. The following values available: json, csv.
	Output string `required:"false" name:"output" default:"json"`
}

type CallSessionInfo struct {
	// The call session history ID.
	CallSessionHistoryId int `json:"call_session_history_id"`
	// The account ID.
	AccountId int `json:"account_id"`
	// The application ID.
	ApplicationId int `json:"application_id"`
	// The user ID.
	UserId int `json:"user_id"`
	// The UTC start date in format YYYY-MM-DD HH:mm:SS
	StartDate string `json:"start_date"`
	// The initiator IP address.
	InitiatorAddress string `json:"initiator_address"`
	// The media server IP address.
	MediaServerAddress string `json:"media_server_address"`
	// The session log URL.
	LogFileUrl string `json:"log_file_url"`
	// The session duration in seconds.
	Duration int `json:"duration"`
	// The binding calls.
	Calls []CallInfo `json:"calls"`
	// The used resorces.
	OtherResourceUsage []ResourceUsage `json:"other_resource_usage"`
	// The binding records.
	Records []Record `json:"records"`
}

type GetCallHistoryResponse struct {
	Result []CallSessionInfo `json:"result"`
	// The total found call session count.
	TotalCount int `json:"total_count"`
	// The returned call session count.
	Count int `json:"count"`
}

func (c *Client) GetCallHistoryByCustomData(appName, data string) (*GetCallHistoryResponse, error) {
	d := url.Values{}
	from := time.Unix(0, 0).UTC()
	to := time.Now().UTC().AddDate(0, 0, 1)
	d.Add("from_date", from.Format("2006-01-02 15:04:05"))
	d.Add("to_date", to.Format("2006-01-02 15:04:05"))
	d.Add("application_name", appName)
	d.Add("with_calls", "true")
	d.Add("call_session_history_custom_data", data)
	if body, err := c.request("GetCallHistory", d); err != nil {
		return nil, err
	} else {
		resp := GetCallHistoryResponse{}
		if err = json.Unmarshal(body, &resp); err != nil {
			return nil, err
		}
		return &resp, nil
	}
}
