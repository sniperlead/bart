package voximplant

// import (
// 	"errors"
// 	"fmt"
// 	"net/url"
// 	"reflect"
// 	"strconv"
// 	"strings"
// 	"time"
// )

// type VoxImplantRequest interface{}

// type DateTime time.Time

// // func (d *DateTime) ToString() {
// // 	return d.Format("2015-12-01 13:13:13")
// // }

// type GetCallHistoryRequest struct {
// 	// The UTC from date filter in format YYYY-MM-DD HH:mm:SS.
// 	FromDate string `required:"true" name:"from_date"`
// 	// The UTC to date filter in format YYYY-MM-DD HH:mm:SS.
// 	ToDate string `required:"true" name:"to_date"`
// 	// The application ID.
// 	ApplicationId int `required:"false" name:"application_id"`
// 	// The application name.
// 	ApplicationName string `required:"false" name:"application_name"`
// 	// The call session history ID list separated by the ';' symbol.
// 	CallSessionHistoryId string `required:"false" name:"call_session_history_id"`
// 	// The user ID list to filter separated by the ';' symbol.
// 	UserId string `required:"false" name:"user_id"`
// 	// The rule name to filter.
// 	RuleName string `required:"false" name:"rule_name"`
// 	// The remote number to filter.
// 	RemoteNumber string `required:"false" name:"remote_number"`
// 	// The local number to filter.
// 	LocalNumber string `required:"false" name:"local_number"`
// 	// The custom_data to filter sessions.
// 	CallSessionHistoryCustomData string `required:"false" name:"call_session_history_custom_data"`
// 	// Set true to get binding calls.
// 	WithCalls bool `required:"false" name:"with_calls" default:"false"`
// 	// Set true to get binding records.
// 	WithRecords bool `required:"false" name:"with_records" default:"false"`
// 	// Set true to get other resources usage.
// 	WithOtherResources bool `required:"false" name:"with_other_resources" default:"false"`
// 	// The child account ID list separated by the ';' symbol or the 'all' value.
// 	ChileAccountId string `required:"false" name:"child_account_id"`
// 	// Set true to get the children account calls only.
// 	ChildrenCallsOnly bool `required:"false" name:"children_calls_only"`
// 	// Set false to get a CSV file without the column names if the output=csv.
// 	WithHeader bool `required:"false" name:"with_header"`
// 	// Set true to get records in the descent order.
// 	DescOrder bool `required:"false" name:"desc_order"`
// 	// The max returning record count.
// 	Count int `required:"false" name:"count" default:"20"`
// 	// The record count to omit.
// 	Offset int `required:"false" name:"offset"`
// 	// The output format. The following values available: json, csv.
// 	Output string `required:"false" name:"output" default:"json"`
// }

// func GetValues(r VoxImplantRequest) error {
// 	values := url.Values{}
// 	errs := InvalidRequestError{}
// 	typ := reflect.TypeOf(r).Elem()
// 	val := reflect.ValueOf(r).Elem()
// 	for i, count := 0, typ.NumField(); i < count; i++ {
// 		f := typ.Field(i)
// 		v := val.Field(i)

// 		// Set default value for a field if it is not set.
// 		dfl := f.Tag.Get("default")
// 		if dfl != "" && v.CanAddr() && v.CanSet() {
// 			switch f.Type.Kind() {
// 			case reflect.String:
// 				v.SetString(dfl)
// 			case reflect.Bool:
// 				v.SetBool(strings.ToLower(dfl) == "true")
// 			case reflect.Int:
// 				if i, err := strconv.ParseInt(dfl, 10, 64); err != nil {
// 					errs[f.Name] = append(errs[f.Name], err)
// 				} else {
// 					v.SetInt(i)
// 				}
// 			}
// 		}

// 		// Check all required fields.
// 		if strings.ToLower(f.Tag.Get("required")) == "true" {
// 			switch f.Type.Kind() {
// 			case reflect.String:
// 				if v.String() == "" {
// 					errs[f.Name] = append(errs[f.Name], errors.New("Required"))
// 				}
// 			}
// 		}

// 		// Add field value to values.
// 		name := f.Tag.Get("name")
// 		switch f.Type.Kind() {
// 		case reflect.String:
// 			if v.String() != "" {
// 				values.Add(name, v.String())
// 			}
// 		case reflect.Int:
// 			if v.Int() != 0 {
// 				values.Add(name, strconv.FormatInt(v.Int(), 10))
// 			}
// 		case reflect.Bool:
// 			values.Add(name, strconv.FormatBool(v.Bool()))
// 		}
// 	}
// 	fmt.Println(values.Encode())
// 	if len(errs) > 0 {
// 		return &errs
// 	}
// 	return nil
// }

// func main() {
// 	r := GetCallHistoryRequest{
// 		FromDate: time.Now().Format("2006-01-02 15:04:05"),
// 		ToDate:   time.Now().Format("2006-01-02 15:04:05"),
// 	}
// 	fmt.Println(r.FromDate)
// 	errors := SetDefaults(&r)
// 	fmt.Println(errors)
// }
