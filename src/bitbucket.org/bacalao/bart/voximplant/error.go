package voximplant

import "fmt"

type ErrorData struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

type APIError struct {
	Data ErrorData `json:"error"`
}

func (e *APIError) Error() string {
	return fmt.Sprintf("Error: %d - %s", e.Data.Code, e.Data.Msg)
}
