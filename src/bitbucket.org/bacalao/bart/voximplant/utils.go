package voximplant

import (
	"errors"
	"fmt"
	"net/url"
	"reflect"
	"strconv"
	"strings"
)

func GetValues(r VoxImplantRequest) error {
	values := url.Values{}
	errs := InvalidRequestError{}
	typ := reflect.TypeOf(r).Elem()
	val := reflect.ValueOf(r).Elem()
	for i, count := 0, typ.NumField(); i < count; i++ {
		f := typ.Field(i)
		v := val.Field(i)

		// Set default value for a field if it is not set.
		dfl := f.Tag.Get("default")
		if dfl != "" && v.CanAddr() && v.CanSet() {
			switch f.Type.Kind() {
			case reflect.String:
				v.SetString(dfl)
			case reflect.Bool:
				v.SetBool(strings.ToLower(dfl) == "true")
			case reflect.Int:
				if i, err := strconv.ParseInt(dfl, 10, 64); err != nil {
					errs[f.Name] = append(errs[f.Name], err)
				} else {
					v.SetInt(i)
				}
			}
		}

		// Check all required fields.
		if strings.ToLower(f.Tag.Get("required")) == "true" {
			switch f.Type.Kind() {
			case reflect.String:
				if v.String() == "" {
					errs[f.Name] = append(errs[f.Name], errors.New("Required"))
				}
			}
		}

		// Add field value to values.
		name := f.Tag.Get("name")
		switch f.Type.Kind() {
		case reflect.String:
			if v.String() != "" {
				values.Add(name, v.String())
			}
		case reflect.Int:
			if v.Int() != 0 {
				values.Add(name, strconv.FormatInt(v.Int(), 10))
			}
		case reflect.Bool:
			values.Add(name, strconv.FormatBool(v.Bool()))
		}
	}

	fmt.Println(values.Encode())
	if len(errs) > 0 {
		return &errs
	}

	return nil
}
