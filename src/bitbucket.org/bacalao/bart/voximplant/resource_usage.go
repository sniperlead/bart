package voximplant

// The resource usage info.
type ResourceUsage struct {
	// The resource usage ID.
	Id int `json:"resource_usage_id"`
	// The resource type.
	Type string `json:"resource_type"`
	// The start resource using UTC time in format: YYYY-MM-DD HH:mm:SS
	UsedAt string `json:"used_at"`
	// The transaction ID.
	TransactionId int `json:"transaction_id"`
	// The call cost.
	Cost float64 `json:"cost"`
	// The description.
	Description string `json:"description"`
}
