package voximplant

import "fmt"

type VoxImplantRequest interface{}

type Response interface {
	error
	IsSuccess() bool
}

type InvalidRequestError map[string][]error

func (e *InvalidRequestError) Error() string {
	return fmt.Sprintf("%v", e)
}

type API_Error struct {
	Code    int    `json:"code"`
	Message string `json:"msg"`
}

type ScenarioInfo struct {
	// The scenario ID.
	ScenarioId int `json:"scenario_id"`
	// The scenario name.
	ScenarioName string `json:"scenario_name"`
	// The scenario editing UTC date in format: YYYY-MM-DD HH:mm:SS
	Modified string `json:"modified"`
	// The scenario text.
	ScenarioScript *string `json:"scenario_script"`
}

type GetScenariosResponse struct {
	Result []ScenarioInfo `json:"result"`
	// The total found scenario count.
	TotalCount int `json:"total_count"`
	// The returned scenario count.
	Count    int        `json:"count"`
	APIError *API_Error `json:"error"`
}

func (r *GetScenariosResponse) IsSuccess() bool {
	return r.APIError == nil
}

func (r *GetScenariosResponse) Error() string {
	if r.APIError == nil {
		return ""
	}
	return r.APIError.Message
}

type StartScenariosResponse struct {
	Result int `json:"result"`
	// The URL to control a new created media session.
	MediaSessionAccessURL string `json:"media_session_access_url"`
	// The URL to check media session.
	MediaSessionCheckURL string     `json:"media_session_check_url"`
	APIError             *API_Error `json:"error"`
}

func (r *StartScenariosResponse) IsSuccess() bool {
	return r.APIError == nil
}

func (r *StartScenariosResponse) Error() string {
	if r.APIError == nil {
		return ""
	}
	return r.APIError.Message
}
