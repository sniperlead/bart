package voximplant

// The call info.
type CallInfo struct {
	// The call history ID.
	Id int `json:"call_id"`
	// The UTC start time in format YYYY-MM-DD HH:mm:SS
	StartTime string `json:"start_time"`
	// The local number.
	LocalNumber string `json:"local_number"`
	// The remote number.
	RemoteNumber string `json:"remote_number"`
	// The remote number type.
	RemoteNumberType string `json:"remote_number_type"`
	// The incoming flag.
	Incoming bool `json:"incoming"`
	// The success flag.
	Successful bool `json:"successful"`
	// The transaction ID.
	TransactionId int `json:"transaction_id"`
	// The media server IP address.
	MediaServerAddress string `json:"media_server_address"`
	// The call duration in seconds.
	Duration int `json:"duration"`
	// The record URL.
	RecordURL string `json:"record_url"`
	// The call cost.
	Cost float64 `json:"cost"`
	// The custom data.
	CustomData string `json:"custom_data"`
}
