package voximplant

import (
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

const MAX_RESPONSE_LENGTH = 10 * 1024 * 1024 // 10 Mb
const API_ENDPOINT = "https://api.voximplant.com/platform_api"

type Client struct {
	// VoxImplant API endpoint.
	Endpoint string

	// One of this property should be set.
	// The account ID.
	AccountId string
	// The account name.
	AccountName string
	// The account email.
	AccountEmail string

	// One of this property should be set.
	// The account API key.
	ApiKey string
	// The account password.
	AccountPassword string
	// The API session ID.
	SessionId string
}

func New(account_id, api_key string) *Client {
	return &Client{AccountId: account_id, ApiKey: api_key}
}

func (c *Client) authData(data url.Values) {
	if c.AccountId != "" {
		data.Add("account_id", c.AccountId)
	} else if c.AccountName != "" {
		data.Add("account_name", c.AccountName)
	} else {
		data.Add("account_email", c.AccountEmail)
	}

	if c.ApiKey != "" {
		data.Add("api_key", c.ApiKey)
	} else if c.AccountPassword != "" {
		data.Add("account_password", c.AccountPassword)
	} else {
		data.Add("session_id", c.SessionId)
	}
}

func (c *Client) Request(action string, data url.Values) ([]byte, error) {
	return c.request(action, data)
}

func (c *Client) request(action string, data url.Values) ([]byte, error) {
	c.authData(data)
	reqBody := strings.NewReader(data.Encode())
	var endpoint string
	if c.Endpoint == "" {
		endpoint = API_ENDPOINT
	} else {
		endpoint = c.Endpoint
	}
	var url string
	if strings.HasSuffix(url, "/") {
		url = fmt.Sprintf("%s%s", endpoint, action)
	} else {
		url = fmt.Sprintf("%s/%s", endpoint, action)
	}
	req, err := http.NewRequest("POST", url, reqBody)
	if err != nil {
		return nil, err
	}

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.ContentLength > MAX_RESPONSE_LENGTH {
		return nil, errors.New(fmt.Sprintf("response length is more then %d", MAX_RESPONSE_LENGTH))
	}
	body, err := ioutil.ReadAll(io.LimitReader(resp.Body, MAX_RESPONSE_LENGTH))
	if err != nil {
		return nil, err
	}
	r := map[string]interface{}{}
	if err = json.Unmarshal(body, &r); err != nil {
		return nil, err
	}
	if _, ok := r["error"]; ok {
		errResp := &APIError{}
		if err = json.Unmarshal(body, &errResp); err != nil {
			return nil, err
		}
		return nil, errResp
	}
	return body, nil
}

func (c *Client) GetScenarios(id string, withScript bool, count, offset int) (*GetScenariosResponse, error) {
	data := make(url.Values)
	if id != "" {
		data.Add("scenario_id", id)
	}
	data.Add("with_script", strconv.FormatBool(withScript))
	if count == 0 {
		count = 20
	}
	data.Add("count", strconv.FormatInt(int64(count), 10))
	data.Add("offset", strconv.FormatInt(int64(offset), 10))
	if body, err := c.request("GetScenarios", data); err != nil {
		return nil, err
	} else {
		resp := GetScenariosResponse{}
		if err = json.Unmarshal(body, &resp); err != nil {
			return nil, err
		}
		return &resp, nil
	}
}

func (c *Client) StartScenarios(username, ruleId, data string) (*StartScenariosResponse, error) {
	d := make(url.Values)
	if username != "" {
		d.Add("user_name", username)
	}
	d.Add("rule_id", ruleId)
	if data != "" {
		d.Add("script_custom_data", data)
	}
	if body, err := c.request("StartScenarios", d); err != nil {
		return nil, err
	} else {
		resp := StartScenariosResponse{}
		if err = json.Unmarshal(body, &resp); err != nil {
			return nil, err
		}
		return &resp, nil
	}
}
