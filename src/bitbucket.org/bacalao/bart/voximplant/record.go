package voximplant

// The record info.
type Record struct {
	// The record ID.
	Id int `json:"record_id"`
	// The start recording UTC time in format: YYYY-MM-DD HH:mm:SS
	StartTime string `json:"start_time"`
	// The transaction ID.
	TransactionId int `json:"transaction_id"`
	// The record name.
	RecordName string `json:"record_name"`
	// The record cost.
	Cost int `json:"cost"`
	// The call duration in seconds.
	Duration int `json:"duration"`
	// The record URL.
	RecordUrl string `json:"record_url"`
}
