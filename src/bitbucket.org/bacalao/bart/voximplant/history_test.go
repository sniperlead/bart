package voximplant

import (
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

var callHistoryResponse string = `{
  "result": [{
    "rule_name": "callback",
    "active": false,
    "application_id": 413943,
    "duration": 62,
    "account_id": 79697,
    "other_resource_usage": [],
    "user_id": 659824,
    "calls": [{
      "duration": 45,
      "transaction_id": 398090100,
      "start_time": "2015-07-21 22:25:44",
      "incoming": false,
      "cost": 0.0328000000,
      "remote_number_type": "pstn",
      "local_number": "",
      "remote_number": "79219315811",
      "custom_data": "55aec69a5d24f68bdc000018",
      "call_id": 35799500,
      "successful": true,
      "direction": "Russia Mobile"
    }, {
      "duration": 29,
      "transaction_id": 398090310,
      "start_time": "2015-07-21 22:25:59",
      "incoming": false,
      "cost": 0.0328000000,
      "remote_number_type": "pstn",
      "local_number": "",
      "remote_number": "79531431135",
      "custom_data": "55aec69a5d24f68bdc000018",
      "call_id": 35799499,
      "successful": true,
      "direction": "Russia Mobile"
    }],
    "initiator_address": "0.0.0.0",
    "call_session_history_id": 47780793,
    "media_server_address": "38.88.16.65",
    "log_file_url": "http:\/\/www-us-01-65.voximplant.com\/logs\/2015\/07\/21\/222527_a11b0d66a42bf9d2.1437517527.145991_38.88.16.65.log",
    "custom_data": "55aec69a5d24f68bdc000018:7dbd4bbb-a4b6-4e60-59a1-ab765dde22a1:79219315811:79531431135",
    "start_date": "2015-07-21 22:25:27"
  }],
  "total_count":1,
  "count":1
}`

var callHistoryErrorResponse = `{
  "error": {
    "msg": "Invalid application name.",
    "code": 107
  }
}`

func Test_GetCallHistoryByCustomData(t *testing.T) {
	assert := assert.New(t)

	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		io.WriteString(w, callHistoryResponse)
	}))
	defer server.Close()

	client := New("fake-account", "fake-key")
	client.Endpoint = server.URL
	resp, err := client.GetCallHistoryByCustomData("fake-app", "fake-data")
	assert.Nil(err)
	assert.Equal(resp.TotalCount, 1)
	assert.Equal(resp.Count, 1)
	assert.Equal(len(resp.Result), 1)
	assert.Equal(len(resp.Result[0].Calls), 2)
}

// client = New("79697", "24101d43-0461-4f0c-8441-e55262a52e45")
// app := "sniperlead.plusit.voximplant.com1"
// data := "55aec69a5d24f68bdc000018:7dbd4bbb-a4b6-4e60-59a1-ab765dde22a1:79219315811:79531431135"
// resp, err = client.GetCallHistoryByCustomData(app, data)
// fmt.Println(err)

// assert.Equal(resp.TotalCount, 1)
// assert.Equal(resp.Count, 1)
// assert.Equal(len(resp.Result), 1)
// assert.Equal(len(resp.Result[0].Calls), 2)

func Test_GetCallHistoryByCustomDataError(t *testing.T) {
	assert := assert.New(t)

	handler := func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		io.WriteString(w, callHistoryErrorResponse)
	}
	server := httptest.NewServer(http.HandlerFunc(handler))
	defer server.Close()

	client := New("fake-account", "fake-key")
	client.Endpoint = server.URL
	resp, err := client.GetCallHistoryByCustomData("fake-app", "fake-data")
	assert.Nil(resp)
	assert.Equal(err.Error(), "Error: 107 - Invalid application name.")
}
