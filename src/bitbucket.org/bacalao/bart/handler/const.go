package handler

// Max request length in bytes.
const MAX_REQUEST_CONTENT_LENGTH = 1024 * 1024 // 1 Mb
