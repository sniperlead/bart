package handler

import (
	"bitbucket.org/bacalao/bart/service"
	"net/http"
)

// Handler for /cb requests, implements 'net/http/Handler' interface.
type Cb struct {
	// Account service.
	accountSvc service.AccountService
	// Domen service.
	domenSvc service.DomenService
	// Request service.
	requestSvc service.RequestService
	// Callback service.
	callbackSvc service.CallbackService
}

func (h Cb) Create(w http.ResponseWriter, r *http.Request) {
}
