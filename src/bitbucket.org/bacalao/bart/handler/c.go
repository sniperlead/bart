package handler

import (
	"bitbucket.org/bacalao/bart/config"
	"bitbucket.org/bacalao/bart/errors"
	"bitbucket.org/bacalao/bart/logger"
	"bitbucket.org/bacalao/bart/storage"
	"encoding/json"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type CRequest struct {
	URI      string `json:"uri"`
	Referer  string `json:"referer"`
	ClientId string `json:"client_id"`

	Host  string `json:"-"`
	Path  string `json:"-"`
	Query string `json:"-"`
}

// /c requests handler
type c struct {
	Pattern string
}

// Validates given http request and converts it to CRequest.
func (h *c) validatePostRequest(r *http.Request) (*CRequest, errors.Error) {
	reqBody, err := ReadRequestBody(r)
	if err != nil {
		return nil, err
	}
	req := CRequest{}
	if err := json.Unmarshal(reqBody, &req); err != nil {
		return nil, errors.NewInvalidRequest()
	}

	// trim all properties
	req.URI = strings.TrimSpace(req.URI)
	req.Referer = strings.TrimSpace(req.Referer)
	req.ClientId = strings.TrimSpace(req.ClientId)

	// TODO: validate ClientId it should IsObjectIdHex.

	// Validate request URI.
	if p, err := url.Parse(req.URI); err != nil {
		return nil, errors.NewInvalidRequest()
	} else {
		req.Host = p.Host
		req.Path = p.Path
		req.Query = p.RawQuery
	}
	return &req, nil
}

func getOrCreateClient(db *mgo.Database, clientId string, accountId bson.ObjectId) (*storage.Client, errors.Error) {
	var client storage.Client

	if clientId != "" {
		if err := db.C("clients").Find(bson.M{
			"id":      bson.ObjectIdHex(clientId),
			"deleted": false,
			"account_id": mgo.DBRef{
				Collection: "accounts",
				Id:         accountId,
			},
		}).One(&client); err != nil {
			if err != mgo.ErrNotFound {
				logger.L.Error("Cannot get account's {id: %s} client {id: %s}: %s",
					accountId, clientId, err)
				return nil, errors.NewInternalServerError()
			}
			goto CREATE_CLIENT
		}
		return &client, nil
	}

CREATE_CLIENT:

	now := time.Now()

	client = storage.Client{
		Id: bson.NewObjectId(),
		AccountRef: mgo.DBRef{
			Collection: "accounts",
			Id:         accountId,
		},
		Deleted:   false,
		CreatedAt: now,
		UpdatedAt: now,
	}
	if err := db.C("clients").Insert(&client); err != nil {
		logger.L.Error("Cannot create client {id: %s} for account {id: %s}: %s",
			clientId, accountId, err)
		return nil, errors.NewInternalServerError()
	}
	return &client, nil
}

func (h *c) post(w http.ResponseWriter, r *http.Request) {
	var err error
	var req *CRequest
	var httpErr errors.Error

	if req, httpErr = h.validatePostRequest(r); httpErr != nil {
		WriteErrorResponse(w, httpErr)
		return
	}

	origin := r.Header.Get("Origin")
	if origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
	}

	session, err := storage.GetSession()
	if err != nil {
		logger.L.Error("Cannot create mongodb session: %s", err)
		WriteErrorResponse(w, errors.NewInternalServerError())
		return
	}
	defer session.Close()

	db := session.DB(config.C.MongoDB.Database)

	// Get domen, to verify that this request is from authorized source (website).
	q := bson.M{"host": req.Host, "deleted": false}
	domen := storage.Domen{}
	if err = db.C("domens").Find(q).One(&domen); err != nil {
		if err == mgo.ErrNotFound {
			logger.L.Debug("Request from %s not handled because host %s unknown",
				req.URI, req.Host)
			WriteErrorResponse(w, errors.NewUnknownHost())
		} else {
			logger.L.Error("Cannot get domen for host %s: %s", req.Host, err)
			WriteErrorResponse(w, errors.NewInternalServerError())
		}
		return
	} else if !domen.IsActive() {
		logger.L.Debug("Doment {id: %s} is not active", domen.Id)
		WriteErrorResponse(w, errors.NewDomenDisabled())
		return
	}

	// Get account, to verify that it is active.
	account := storage.Account{}
	q = bson.M{"deleted": false}
	if err = db.FindRef(&domen.AccountRef).Select(q).One(&account); err != nil {
		if err == mgo.ErrNotFound {
			logger.L.Error("Account for domen {id: %s, host: %s} doesn't exist",
				domen.Id, domen.Host)
		} else {
			logger.L.Error("Cannot get account %s: %s", domen.AccountRef, err)
		}
		WriteErrorResponse(w, errors.NewInternalServerError())
		return
	} else if !account.IsActive() {
		logger.L.Debug("Account {id: %s} is not active", account.Id)
		WriteErrorResponse(w, errors.NewAccountDisabled())
		return
	}

	resp := make(map[string]interface{})
	now := time.Now().UTC()

	client, httpErr := getOrCreateClient(db, req.ClientId, account.Id)
	if httpErr != nil {
		WriteErrorResponse(w, httpErr)
		return
	}

	// Create request.
	request := storage.Request{
		Id: bson.NewObjectId(),
		DomenRef: mgo.DBRef{
			Collection: "domens",
			Id:         domen.Id,
		},
		AccountRef: mgo.DBRef{
			Collection: "accounts",
			Id:         account.Id,
		},
		ClientRef: mgo.DBRef{
			Collection: "clients",
			Id:         client.Id,
		},
		RemoteAddr: r.RemoteAddr,
		URI:        req.URI,
		Host:       req.Host,
		Path:       req.Path,
		Query:      req.Query,
		Referer:    req.Referer,
		Headers: storage.Headers{
			UserAgent:      strings.TrimSpace(r.Header.Get("User-Agent")),
			AcceptLanguage: strings.TrimSpace(r.Header.Get("Accept-Language")),
		},
		CreatedAt: now,
		UpdatedAt: now,
	}
	if err = db.C("requests").Insert(&request); err != nil {
		logger.L.Error("Cannot create request {uri: %s} to domen {id: %s}: %s",
			req.URI, domen.Id, err)
		WriteErrorResponse(w, errors.NewInternalServerError())
		return
	}

	resp["id"] = request.Id.Hex()
	resp["client_id"] = client.Id.Hex()
	resp["rules"] = domen.Rules
	WriteJSON(w, resp)
}

func (h c) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	logger.L.Debug("%s %s %s", r.Proto, r.Method, r.URL)

	switch r.Method {
	case "POST":
		h.post(w, r)
	case "OPTIONS":
		DefaultOptionsHandler(w, r)
	default:
		WriteErrorResponse(w, errors.NewMethodNotAllowed())
	}
}

var C c = c{
	Pattern: "/c",
}
