package handler

import (
	"bitbucket.org/bacalao/bart/config"
	"bitbucket.org/bacalao/bart/errors"
	"bitbucket.org/bacalao/bart/logger"
	"bitbucket.org/bacalao/bart/storage"
	"encoding/json"
	mgo "gopkg.in/mgo.v2"
	bson "gopkg.in/mgo.v2/bson"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

func ReadRequestBody(r *http.Request) ([]byte, errors.Error) {
	if r.Method == "POST" || r.Method == "PUT" {
		// Get request content length.
		var err error
		var content_length int64
		content_length_str := strings.TrimSpace(r.Header.Get("Content-Length"))
		if content_length_str == "" {
			content_length = int64(MAX_REQUEST_CONTENT_LENGTH)
		} else {
			content_length, err = strconv.ParseInt(content_length_str, 10, 64)
			if err != nil || content_length < 0 {
				return nil, errors.NewLengthRequired()
			}
			if content_length > MAX_REQUEST_CONTENT_LENGTH {
				return nil, errors.NewRequestEntityTooLarge()
			}
		}
		if content_length > 0 {
			body, err := ioutil.ReadAll(io.LimitReader(r.Body, content_length))
			if err != nil && err != io.EOF {
				logger.L.Error("Request body reading error: %s", err)
				return nil, errors.NewInvalidRequest()
			}
			return body, nil
		}
	}
	return nil, nil
}

func WriteErrorResponse(w http.ResponseWriter, httpErr errors.Error) {
	w.Header().Set("Content-Type", "application/json")
	if body, err := json.Marshal(httpErr); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(errors.INTERNAL_SERVER_ERROR))
	} else {
		w.WriteHeader(httpErr.Status())
		w.Write(body)
	}
}

func WriteJSON(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	if body, err := json.Marshal(data); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(errors.INTERNAL_SERVER_ERROR))
	} else {
		w.WriteHeader(http.StatusOK)
		w.Write(body)
	}
}

// Default handler for OPTIONS requests, it verifies that:
//   * request has Origin header and it's valid;
//   * domen for request Origin header exist;
//   * account for request domen is active.
func DefaultOptionsHandler(w http.ResponseWriter, r *http.Request) {
	origin := r.Header.Get("Origin")
	if origin == "" {
		logger.L.Debug("OPTIONS request without origin")
		WriteErrorResponse(w, errors.NewInvalidRequest())
		return
	}

	if strings.ToLower(origin) != "null" {
		var p *url.URL
		var err error

		if p, err = url.Parse(origin); err != nil || p.Host == "" {
			if err != nil {
				logger.L.Debug("Invalid request Origin %s: %s", origin, err)
			} else {
				logger.L.Debug("Invalid request Origin %s host is not given", origin)
			}
			WriteErrorResponse(w, errors.NewInvalidRequest())
			return
		}

		// Create mongodb session.
		var session *mgo.Session
		if session, err = storage.GetSession(); err != nil {
			logger.L.Error("Cannot create mongodb session: %s", err)
			WriteErrorResponse(w, errors.NewInternalServerError())
			return
		}
		defer session.Close()
		db := session.DB(config.C.MongoDB.Database)

		// Get Origin's domen from database and verify that it is allowed.
		logger.L.Info("%s host: %s path: %s", p, p.Host, p.Path)
		domen := storage.Domen{}
		if err = db.C("domens").Find(bson.M{
			"host":    p.Host,
			"deleted": false,
		}).One(&domen); err != nil {
			if err == mgo.ErrNotFound {
				logger.L.Debug("Unknown origin host %s", p.Host)
				WriteErrorResponse(w, errors.NewUnknownHost())
			} else {
				logger.L.Error("Cannot get domen for host %s: %s", p.Host, err)
				WriteErrorResponse(w, errors.NewInternalServerError())
			}
			return
		} else if !domen.IsActive() {
			logger.L.Debug("Doment {id: %s} is not active", domen.Id)
			WriteErrorResponse(w, errors.NewDomenDisabled())
			return
		}

		// Verify that account is active.
		account := storage.Account{}
		if err = db.FindRef(&domen.AccountRef).Select(bson.M{
			"deleted": false,
		}).One(&account); err != nil {
			if err == mgo.ErrNotFound {
				logger.L.Error("Account for domen {id: %s, host: %s} doesn't exist",
					domen.Id, domen.Host)
			} else {
				logger.L.Error("Cannot get account %s: %s", domen.AccountRef, err)
			}
			WriteErrorResponse(w, errors.NewInternalServerError())
			return
		} else if !account.IsActive() {
			logger.L.Debug("Account {id: %s} is not active", account.Id)
			WriteErrorResponse(w, errors.NewAccountDisabled())
			return
		}
	}

	w.Header().Set("Access-Control-Allow-Origin", origin)
	w.Header().Set("Access-Control-Allow-Methods", "POST, PUT, OPTIONS")

	allowedHeaders := r.Header.Get("Access-Control-Request-Headers")
	if allowedHeaders != "" {
		w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
	}

	w.Header().Set("Access-Control-Allow-Credentials", "false")
	w.WriteHeader(http.StatusOK)
}

func LogRequest(r *http.Request) {
	logger.L.Debug("%s %s %s", r.Proto, r.Method, r.URL)
}
