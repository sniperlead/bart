package errors

import "net/http"

func NewAccountDisabledError() *HttpError {
	return &HttpError{
		Status:  http.StatusBadRequest,
		Code:    "AccountDisabled",
		Message: "Account disabled",
	}
}

func NewDomenDisabledError() *HttpError {
	return &HttpError{
		Status:  http.StatusBadRequest,
		Code:    "DomenDisabled",
		Message: "Domen disabled",
	}
}
