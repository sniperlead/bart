package errors

import (
	"fmt"
	"net/http"
)

type HttpError struct {
	Status  int               `json:"status"`
	Code    string            `json:"code"`
	Message string            `json:"error"`
	Errors  map[string]string `json:"errors,omitempty"`
}

func (e *HttpError) Error() string {
	return fmt.Sprintf("%s (%d) - %s", e.Status, e.Code, e.Message)
}

func New(status int, code, message string) *HttpError {
	return &HttpError{
		Status:  status,
		Code:    code,
		Message: message,
	}
}

func NewLengthRequired() *HttpError {
	return &HttpError{
		Status:  http.StatusLengthRequired,
		Code:    "LengthRequired",
		Message: "Content-Length header required",
	}
}

func NewStatusRequestEntityTooLarge() *HttpError {
	return &HttpError{
		Status:  http.StatusRequestEntityTooLarge,
		Code:    "RequestEntityTooLarge",
		Message: "Request entity too large",
	}
}

func NewInvalidRequest() *HttpError {
	return &HttpError{
		Status:  http.StatusBadRequest,
		Code:    "InvalidRequest",
		Message: "Invalid request",
	}
}

func NewUnknownHost() *HttpError {
	return &HttpError{
		Status:  http.StatusNotFound,
		Code:    "UnknownHost",
		Message: "Unknown request host",
	}
}

func NewInvalidReferer() *HttpError {
	return &HttpError{
		Status:  http.StatusBadRequest,
		Code:    "InvalidReferer",
		Message: "Invalid request referer",
	}
}

func NewValidationError(errors map[string]string) *HttpError {
	return &HttpError{
		Status: http.StatusBadRequest,
		Code:   "ValidationError",
		Errors: errors,
	}
}

func NewMethodNotAllowedError() *HttpError {
	return &HttpError{
		Status:  http.StatusMethodNotAllowed,
		Code:    "MethodNotAllowed",
		Message: "Method not allowed",
	}
}

func NewInternalServerError() *HttpError {
	return &HttpError{
		Status:  http.StatusInternalServerError,
		Code:    "InternalServerError",
		Message: "Internal server error",
	}
}

const INTERNAL_SERVER_ERROR = `{"status":500,"code":"InternalServerError","error":"Internal server error"}`
