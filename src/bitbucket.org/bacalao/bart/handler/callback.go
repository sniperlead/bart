package handler

// import (
// 	"bitbucket.org/bacalao/bart/config"
// 	"bitbucket.org/bacalao/bart/errors"
// 	"bitbucket.org/bacalao/bart/logger"
// 	"bitbucket.org/bacalao/bart/service"
// 	"bitbucket.org/bacalao/bart/storage"
// 	"bitbucket.org/bacalao/bart/utils"
// 	"bitbucket.org/bacalao/bart/voximplant"
// 	"encoding/json"
// 	bson "gopkg.in/mgo.v2/bson"
// 	"net/http"
// 	"strings"
// )

// type CallbackPOST struct {
// 	// Request id.
// 	RequestId string `json:"request_id"`
// 	// Client phone number to call.
// 	PhoneNumber string `json:"phone_number"`
// }

// // Validates given POST HTTP request and converts it to CallPOST.
// func ValidatePOSTRequest(r *http.Request) (*CallbackPOST, errors.Error) {
// 	var body []byte
// 	var err errors.Error

// 	if body, err = ReadRequestBody(r); err != nil {
// 		return nil, err
// 	}

// 	req := CallbackPOST{}
// 	if err := json.Unmarshal(body, &req); err != nil {
// 		logger.L.Debug("POST request parsing error: %s", err)
// 		return nil, errors.NewInvalidRequest()
// 	}

// 	// Trim all string properties.
// 	req.RequestId = strings.TrimSpace(req.RequestId)
// 	req.PhoneNumber = strings.TrimSpace(req.PhoneNumber)

// 	errs := make(map[string]string)
// 	// Validate that RequestId id valid ObjectId.
// 	if !bson.IsObjectIdHex(req.RequestId) {
// 		errs["request_id"] = "Invalid id."
// 	}
// 	// Validate that PhoneNumber is given.
// 	if req.PhoneNumber == "" {
// 		errs["phone_number"] = "Required."
// 	}
// 	// TODO: validate phone number

// 	if len(errs) != 0 {
// 		return nil, errors.NewValidationError(errs)
// 	}
// 	return &req, nil
// }

// type Call struct {
// 	Status    storage.CallStatus `json:"status"`
// 	StartTime int64              `json:"start_time"`
// 	EndTime   int64              `json:"end_time"`
// 	Duration  int64              `json:"duration"`
// 	Cost      float32            `json:"cost"`
// }

// func (c *Call) IsValidStatus() bool {
// 	return (c.Status == storage.CALL_CONNECTED ||
// 		c.Status == storage.CALL_DISCONNECTED ||
// 		c.Status == storage.CALL_FAILED)
// }

// func (c *Call) AddSetProperties(set bson.M, prefix string) {
// 	if c.Status != "" {
// 		set[prefix+".status"] = c.Status
// 	}
// 	if c.StartTime != 0 {
// 		set[prefix+".start_time"] = utils.JSTimeToTime(c.StartTime).UTC()
// 	}
// 	if c.EndTime != 0 {
// 		set[prefix+".end_time"] = utils.JSTimeToTime(c.EndTime).UTC()
// 	}
// 	if c.Duration != 0 {
// 		set[prefix+".duration"] = c.Duration
// 	}
// 	if c.Cost != 0.0 {
// 		set[prefix+".cost"] = c.Cost
// 	}
// }

// type CallbackPUT struct {
// 	// Callback id.
// 	Id bson.ObjectId `json:"-"`

// 	// Callback token for updates.
// 	Token string `json:"token"`

// 	AccountCall *Call `json:"account_call"`
// 	ClientCall  *Call `json:"client_call"`
// }

// func ValidatePUTRequest(r *http.Request) (*CallbackPUT, errors.Error) {
// 	var body []byte
// 	var err errors.Error
// 	if body, err = ReadRequestBody(r); err != nil {
// 		return nil, err
// 	}

// 	req := CallbackPUT{}
// 	if err := json.Unmarshal(body, &req); err != nil {
// 		logger.L.Debug("PUT request parsing error: %s", err)
// 		return nil, errors.NewInvalidRequest()
// 	}

// 	id := strings.TrimSuffix(strings.TrimPrefix(r.URL.Path, "/callback/"), "/")
// 	// Validate that callback id is valid ObjectId.
// 	if !bson.IsObjectIdHex(id) {
// 		logger.L.Debug("Invalid callback id: %s", id)
// 		return nil, errors.NewNotFound()
// 	}
// 	req.Id = bson.ObjectIdHex(id)

// 	errs := make(map[string]string)

// 	if req.Token == "" {
// 		errs["token"] = "Required."
// 	}

// 	if req.AccountCall != nil {
// 		if req.AccountCall.Status != "" && !req.AccountCall.IsValidStatus() {
// 			errs["account_call.status"] = "Invalid status value."
// 		}
// 	}

// 	if req.ClientCall != nil {
// 		if req.ClientCall.Status != "" && !req.ClientCall.IsValidStatus() {
// 			errs["client_call.status"] = "Invalid status value."
// 		}
// 	}

// 	if len(errs) != 0 {
// 		return nil, errors.NewValidationError(errs)
// 	}
// 	return &req, nil
// }

// // Handler for /call requests, implements 'net/http/Handler' interface.
// type CallbackHandler struct {
// 	// Account service.
// 	accountSvc service.AccountService
// 	// Domen service.
// 	domenSvc service.DomenService
// 	// Request service.
// 	requestSvc service.RequestService
// 	// Callback service.
// 	callbackSvc service.CallbackService
// }

// func POST(req *CallbackPOST, requestService service.RequestService,
// 	accountService service.AccountService,
// 	domenService service.DomenService,
// 	callbackService service.CallbackService,
// 	voximplantClient *voximplant.Client) errors.Error {
// 	var err errors.Error
// 	var acc *storage.Account
// 	var request *storage.Request
// 	var cb *storage.Callback

// 	// Get request by id.
// 	if request, err = requestService.GetById(req.RequestId); err != nil {
// 		if err.Status() != http.StatusNotFound {
// 			return err
// 		}
// 		return errors.NewValidationError(
// 			map[string]string{"request_id": "Not found"})
// 	}

// 	// Verify that account is active.
// 	if acc, err = accountService.GetByRef(&request.AccountRef); err != nil {
// 		return err
// 	} else if !acc.IsActive() {
// 		return errors.NewAccountDisabled()
// 	}

// 	// Verify that domen is active.
// 	if domen, err := domenService.GetByRef(&request.DomenRef); err != nil {
// 		return err
// 	} else if !domen.IsActive() {
// 		return errors.NewDomenDisabled()
// 	}

// 	// Verify that there is no call for this request.
// 	if _, err := callbackService.GetByRequestId(request.Id); err == nil {
// 		return errors.NewValidationError(map[string]string{"request_id": "call for this request already exist"})
// 	} else if err.Status() != http.StatusNotFound {
// 		return errors.NewInternalServerError()
// 	}

// 	// TODO: verify that there was no calls to this phone number from account for last 30 minutes.

// 	// Create callback.
// 	cb, err = callbackService.Create(request, acc.PhoneNumber, req.PhoneNumber)
// 	if err != nil {
// 		return err
// 	}

// 	logger.L.Debug("Issue voximplant request for request {id: %s} callback {id: %s}", request.Id.Hex(), cb.Id.Hex())
// 	// Start VoxImplant scenario.
// 	if resp, err := voximplantClient.StartScenarios("", config.C.VoxImplant.Username, config.C.VoxImplant.RuleId, cb.ExternalId, ""); err != nil {
// 		logger.L.Debug("VoxImplant response: %s", resp)
// 		logger.L.Debug("VoxImplant error: %s", err)
// 	}

// 	return nil
// }

// func PUT(req *CallbackPUT, callbackService service.CallbackService) errors.Error {
// 	// Verify that callback exists and token is valid.
// 	if cb, err := callbackService.GetById(req.Id); err != nil {
// 		return err
// 	} else if cb.Token != req.Token {
// 		logger.L.Debug("Invalid callback token: %s", req.Token)
// 		// TODO: Return Invalid token error.
// 		return nil
// 	}

// 	set := bson.M{}
// 	if req.AccountCall != nil {
// 		req.AccountCall.AddSetProperties(set, "account_call")
// 	}
// 	if req.ClientCall != nil {
// 		req.ClientCall.AddSetProperties(set, "client_call")
// 	}
// 	if len(set) == 0 {
// 		return nil
// 	}
// 	return callbackService.Update(req.Id, set)
// }

// func (h *CallbackHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
// 	LogRequest(r)

// 	if r.Method == "OPTIONS" {
// 		DefaultOptionsHandler(w, r)
// 		return
// 	}

// 	session, mgoError := storage.GetSession()
// 	if mgoError != nil {
// 		logger.L.Error("Cannot create mongodb session: %s", mgoError)
// 		WriteErrorResponse(w, errors.NewInternalServerError())
// 		return
// 	}
// 	defer session.Close()

// 	db := session.DB(config.C.MongoDB.Database)

// 	r.URL.Path = strings.TrimSuffix(r.URL.Path, "/")
// 	if r.Method == "POST" && r.URL.Path != "/callback" {
// 		r.Method = "PUT"
// 	}

// 	if r.Method == "POST" {
// 		// TODO: validate origin!
// 		origin := r.Header.Get("Origin")
// 		if origin != "" {
// 			w.Header().Set("Access-Control-Allow-Origin", origin)
// 		}
// 		req, err := ValidatePOSTRequest(r)
// 		if err != nil {
// 			WriteErrorResponse(w, err)
// 			return
// 		}
// 		if err := POST(req, service.NewRequestService(storage.NewRequestDAO(db)),
// 			service.NewAccountService(storage.NewAccountDAO(db)),
// 			service.NewDomenService(storage.NewDomenDAO(db)),
// 			service.NewCallbackService(storage.NewCallbackDAO(db)),
// 			voximplant.New(config.C.VoxImplant.AccountId, config.C.VoxImplant.APIKey)); err != nil {
// 			WriteErrorResponse(w, err)
// 		}
// 	} else if r.Method == "PUT" {
// 		req, err := ValidatePUTRequest(r)
// 		if err != nil {
// 			WriteErrorResponse(w, err)
// 			return
// 		}
// 		err = PUT(req, service.NewCallbackService(storage.NewCallbackDAO(db)))
// 		if err != nil {
// 			WriteErrorResponse(w, err)
// 		}
// 	} else {
// 		WriteErrorResponse(w, errors.NewMethodNotAllowed())
// 	}
// }

// var DefaultCallbackHandler CallbackHandler = CallbackHandler{}
