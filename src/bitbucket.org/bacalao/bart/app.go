package bart

import (
	cfg "bitbucket.org/bacalao/bart/config"
	ctrl "bitbucket.org/bacalao/bart/controller"
	"bitbucket.org/bacalao/bart/logger"
	"bitbucket.org/bacalao/bart/service"
	"bitbucket.org/bacalao/bart/storage"
	"bitbucket.org/bacalao/bart/voximplant"
	"bitbucket.org/bacalao/bart/worker"
	"github.com/gorilla/mux"
	"net/http"
)

type App struct {
	cfg *cfg.AppConfig
}

func (app *App) Start() int {
	client := voximplant.New(app.cfg.VoxImplant.AccountId, app.cfg.VoxImplant.APIKey)

	sessionSvc := service.NewSessionService(&app.cfg.MongoDB)
	accountSvc := service.NewAccountService(storage.NewAccountDAO())
	domenSvc := service.NewDomenService(storage.NewDomenDAO())
	requestSvc := service.NewRequestService(storage.NewRequestDAO())
	cbSvc := service.NewCallbackService(storage.NewCallbackDAO())
	clientSvc := service.NewClientService(storage.NewClientDAO())
	voxImplantSvc := service.NewVoxImplantService(&app.cfg.VoxImplant, client)

	updateCbWrk := worker.NewUpdateCallbackWorker(sessionSvc, cbSvc, voxImplantSvc)
	syncCbWrk := worker.NewSyncCallbacksWorker(sessionSvc, cbSvc, updateCbWrk)

	syncCbWrk.Start()
	updateCbWrk.Start()

	opts := ctrl.NewOptions(sessionSvc, accountSvc, domenSvc)
	r := ctrl.NewR(sessionSvc, accountSvc, domenSvc, clientSvc, requestSvc)
	cb := ctrl.NewCb(sessionSvc, accountSvc, domenSvc, requestSvc, cbSvc,
		voxImplantSvc, updateCbWrk)

	router := mux.NewRouter()
	router.HandleFunc("/r", opts.Options).Methods("OPTIONS")
	router.HandleFunc("/r", r.Create).Methods("POST").Name("CreateR")

	router.HandleFunc("/cb", opts.Options).Methods("OPTIONS")
	router.HandleFunc("/cb", cb.Create).Methods("POST").Name("CreateCb")
	router.HandleFunc("/cb/{id}", opts.Options).Methods("OPTIONS")
	router.HandleFunc("/cb/{id}", cb.Update).Methods("POST", "PUT").Name("UpdateCb")

	http.Handle("/", router)

	addr := app.cfg.HTTP.GetAddr()
	logger.L.Info("Listening on %s", addr)
	if err := http.ListenAndServe(addr, nil); err != nil {
		return 1
	}

	return 0
}

func NewApp(cfg *cfg.AppConfig) *App {
	return &App{cfg: cfg}
}
