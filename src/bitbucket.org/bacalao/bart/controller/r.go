package controller

import (
	"bitbucket.org/bacalao/bart/errors"
	"bitbucket.org/bacalao/bart/logger"
	"bitbucket.org/bacalao/bart/service"
	"encoding/json"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"net/url"
	"strings"
)

// Request controller.
type R struct {
	// mongodb session service.
	sessionSvc service.SessionService
	// Account service.
	accountSvc service.AccountService
	// Domen service.
	domenSvc service.DomenService
	// Client service.
	clientSvc service.ClientService
	// Request service.
	requestSvc service.RequestService
}

type rCreate struct {
	URI      string `json:"uri"`
	Referer  string `json:"referer"`
	ClientId string `json:"client_id"`
}

func (c *rCreate) Normalize() {
	// trim all properties
	c.URI = strings.TrimSpace(c.URI)
	c.Referer = strings.TrimSpace(c.Referer)
	c.ClientId = strings.TrimSpace(c.ClientId)
}

func (c rCreate) Validate() errors.Error {
	errs := make(map[string]string)
	// Validate that RequestId id valid ObjectId.
	if c.ClientId != "" && !bson.IsObjectIdHex(c.ClientId) {
		errs["client_id"] = "Invalid id."
	}
	if _, err := url.Parse(c.URI); err != nil {
		errs["uri"] = fmt.Sprintf("Invalid URI: %s", err.Error())
	}
	if len(errs) != 0 {
		return errors.NewValidationError(errs)
	}
	return nil
}

// Converts a given HTTP request object to rCreate and also validates it.
func GetRCreate(r *http.Request) (*rCreate, errors.Error) {
	var body []byte
	var err errors.Error
	if body, err = ReadRequestBody(r); err != nil {
		return nil, err
	}
	req := &rCreate{}
	if err := json.Unmarshal(body, req); err != nil {
		logger.L.Debug("POST request parsing error: %s", err)
		return nil, errors.NewInvalidRequest()
	}
	req.Normalize()
	if err := req.Validate(); err != nil {
		return nil, err
	}
	return req, nil
}

func (c R) Create(w http.ResponseWriter, r *http.Request) {
	LogRequest(r)

	origin := r.Header.Get("Origin")
	if origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
	}

	data, err := GetRCreate(r)
	if err != nil {
		WriteErrorResponse(w, err)
		return
	}
	pURI, er := url.Parse(data.URI)
	if er != nil {
		WriteErrorResponse(w, errors.NewInvalidRequest())
		return
	}

	s, err := c.sessionSvc.GetSession()
	if err != nil {
		WriteErrorResponse(w, err)
		return
	}
	defer s.Close()
	db := s.DB("")

	// Verify that domen exist and it's active.
	domen, err := c.domenSvc.GetByHost(db, pURI.Host)
	if err != nil {
		WriteErrorResponse(w, err)
		return
	}

	// Get account, to verify that it is active.
	account, err := c.accountSvc.GetByRef(db, &domen.AccountRef)
	if err != nil {
		WriteErrorResponse(w, err)
		return
	}

	// Get or create client for request.
	var clientId bson.ObjectId
	if data.ClientId == "" {
		clientId = bson.NewObjectId()
	} else {
		clientId = bson.ObjectIdHex(data.ClientId)
	}
	if _, err := c.clientSvc.GetOrCreate(db, &clientId); err != nil {
		WriteErrorResponse(w, err)
		return
	}

	// Create request.
	req, err := c.requestSvc.Create(db, account.Id, domen.Id, clientId, r,
		data.URI, pURI.Host, pURI.Path, pURI.RawQuery, data.Referer)
	if err != nil {
		WriteErrorResponse(w, err)
		return
	}

	WriteJSON(w, map[string]interface{}{
		"id":        req.Id.Hex(),
		"client_id": clientId.Hex(),
		"rules":     domen.Rules,
	})
}

func NewR(
	session service.SessionService,
	account service.AccountService,
	domen service.DomenService,
	client service.ClientService,
	request service.RequestService,
) *R {
	return &R{
		sessionSvc: session,
		accountSvc: account,
		domenSvc:   domen,
		clientSvc:  client,
		requestSvc: request,
	}
}
