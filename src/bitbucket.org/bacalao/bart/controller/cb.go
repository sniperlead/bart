package controller

import (
	"bitbucket.org/bacalao/bart/errors"
	"bitbucket.org/bacalao/bart/logger"
	"bitbucket.org/bacalao/bart/service"
	"bitbucket.org/bacalao/bart/utils"
	"bitbucket.org/bacalao/bart/worker"
	"encoding/json"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"strings"
)

// Callback controller.
type Cb struct {
	// mongodb session service.
	sessionSvc service.SessionService
	// Account service.
	accountSvc service.AccountService
	// Domen service.
	domenSvc service.DomenService
	// Request service.
	requestSvc service.RequestService
	// Callback service.
	callbackSvc service.CallbackService
	// VoxImplant service.
	voxImplantSvc service.VoxImplantService
	// Update callback worker.
	updateCbWrk worker.UpdateCallbackWorker
}

type cbCreate struct {
	// Request id.
	RequestId string `json:"request_id"`
	// Client phone number to call.
	PhoneNumber string `json:"phone_number"`
}

// Trim all string properties.
func (c *cbCreate) Normalize() {
	c.RequestId = strings.TrimSpace(c.RequestId)
	c.PhoneNumber = strings.TrimSpace(c.PhoneNumber)
}

func (c cbCreate) Validate() errors.Error {
	errs := make(map[string]string)
	// Validate that RequestId id valid ObjectId.
	if !bson.IsObjectIdHex(c.RequestId) {
		errs["request_id"] = "Invalid id."
	}
	// Validate that PhoneNumber is given.
	if c.PhoneNumber == "" {
		errs["phone_number"] = "Required."
	}
	// TODO: validate phone number
	if len(errs) != 0 {
		return errors.NewValidationError(errs)
	}
	return nil
}

// Converts a given HTTP request to cbCreate and also validates it.
func GetCbCreate(r *http.Request) (*cbCreate, errors.Error) {
	var body []byte
	var err errors.Error
	if body, err = ReadRequestBody(r); err != nil {
		return nil, err
	}
	req := &cbCreate{}
	if err := json.Unmarshal(body, req); err != nil {
		logger.L.Debug("POST request parsing error: %s", err)
		return nil, errors.NewInvalidRequest()
	}
	req.Normalize()
	if err := req.Validate(); err != nil {
		return nil, err
	}
	return req, nil
}

func (c Cb) Create(w http.ResponseWriter, r *http.Request) {
	LogRequest(r)

	origin := r.Header.Get("Origin")
	if origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
	}
	// TODO: validate origin by domen service.

	data, err := GetCbCreate(r)
	if err != nil {
		WriteErrorResponse(w, err)
		return
	}

	s, err := c.sessionSvc.GetSession()
	if err != nil {
		WriteErrorResponse(w, err)
		return
	}
	defer s.Close()
	db := s.DB("")

	// Get request by id.
	req, err := c.requestSvc.GetById(db, data.RequestId)
	if err != nil {
		if err.Status() == http.StatusNotFound {
			err = errors.NewValidationError(map[string]string{
				"request_id": "Not found",
			})
		}
		WriteErrorResponse(w, err)
		return
	}

	// Verify that account exist and it's active.
	acc, err := c.accountSvc.GetByRef(db, &req.AccountRef)
	if err != nil {
		WriteErrorResponse(w, err)
		return
	}

	// Verify that domen exist and it's active.
	if _, err := c.domenSvc.GetByRef(db, &req.DomenRef); err != nil {
		WriteErrorResponse(w, err)
		return
	}

	// Verify that there is no call for this request.
	if _, err := c.callbackSvc.GetByRequestId(db, req.Id); err == nil {
		err = errors.NewValidationError(map[string]string{
			"request_id": "call for this request already exist",
		})
		WriteErrorResponse(w, err)
		return
	} else if err.Status() != http.StatusNotFound {
		WriteErrorResponse(w, err)
		return
	}

	// TODO: verify that there was no calls to this phone number from account for last 30 minutes.

	// Create callback.
	cb, err := c.callbackSvc.Create(db, req, acc.PhoneNumber, data.PhoneNumber)
	if err != nil {
		WriteErrorResponse(w, err)
		return
	}

	// Start VoxImplant scenario.
	if _, err := c.voxImplantSvc.StartScenario(cb.ExternalId); err != nil {
		// TODO: set cb.Error
		WriteErrorResponse(w, err)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

type callError struct {
	Code   int    `json:"code"`
	Reason string `json:"reason"`
}

type call struct {
	// Flag that indicates if call was successful.
	Successful *bool `json:"successful"`
	// Call start time.
	StartTime *int64 `json:"start_time"`
	// Call duration.
	Duration *int64 `json:"duration"`
	// Call cost.
	Cost *float32 `json:"cost"`
	// Flag that indicates if voicemail was detectd.
	Voicemail *bool `json:"voicemail"`
	// Voicemail pattern if voicemail was detected.
	VoicemailPattern *string `json:"voicemail_pattern"`
	// Call was rejected due timeout.
	Timeout *bool `json:"timeout"`
	// Call error.
	Error *callError `json:"error"`
}

func addCallChanges(c *call, changes bson.M, prefix string) {
	if c != nil {
		if c.Successful != nil {
			changes[prefix+".successful"] = c.Successful
		}
		if c.StartTime != nil && *c.StartTime > 0 {
			changes[prefix+".start_time"] = utils.JSTimeToTime(*c.StartTime).UTC()
		}
		if c.Duration != nil && *c.Duration > 0 {
			changes[prefix+".duration"] = c.Duration
		}
		if c.Cost != nil && *c.Cost >= 0.0 {
			changes[prefix+".cost"] = c.Cost
		}
		if c.Voicemail != nil {
			changes[prefix+".voicemail"] = c.Voicemail
		}
		if c.VoicemailPattern != nil {
			changes[prefix+".voicemail_pattern"] = c.VoicemailPattern
		}
		if c.Timeout != nil {
			changes[prefix+".timeout"] = c.Timeout
		}
		if c.Error != nil {
			changes[prefix+".error"] = c.Error
		}
	}
}

type cbUpdate struct {
	// Callback token for updates.
	Token string `json:"token"`

	Terminated  *bool `json:"terminated"`
	AccountCall *call `json:"account_call"`
	ClientCall  *call `json:"client_call"`
}

// Trim all string properties.
func (c *cbUpdate) Normalize() {
	c.Token = strings.TrimSpace(c.Token)
}

func (c cbUpdate) Validate() errors.Error {
	errs := make(map[string]string)
	if c.Token == "" {
		errs["token"] = "Required."
	}
	if len(errs) != 0 {
		return errors.NewValidationError(errs)
	}
	return nil
}

// Converts a given HTTP request to cbUpdate and also validates it.
func GetCbUpdate(r *http.Request) (*cbUpdate, errors.Error) {
	var body []byte
	var err errors.Error
	if body, err = ReadRequestBody(r); err != nil {
		return nil, err
	}
	req := &cbUpdate{}
	if err := json.Unmarshal(body, req); err != nil {
		logger.L.Debug("PUT request parsing error: %s", err)
		return nil, errors.NewInvalidRequest()
	}
	req.Normalize()
	if err := req.Validate(); err != nil {
		return nil, err
	}
	return req, nil
}

func (c Cb) Update(w http.ResponseWriter, r *http.Request) {
	LogRequest(r)

	origin := r.Header.Get("Origin")
	if origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
	}

	s, err := c.sessionSvc.GetSession()
	if err != nil {
		WriteErrorResponse(w, err)
		return
	}
	defer s.Close()
	db := s.DB("")

	vars := mux.Vars(r)
	// Check if callback with a given id exist.
	id := vars["id"]
	if !bson.IsObjectIdHex(id) {
		WriteErrorResponse(w, errors.NewNotFound())
		return
	}
	cbId := bson.ObjectIdHex(id)
	cb, err := c.callbackSvc.GetById(db, cbId)
	if err != nil {
		WriteErrorResponse(w, err)
		return
	}

	data, err := GetCbUpdate(r)
	if err != nil {
		WriteErrorResponse(w, err)
		return
	}

	if cb.Token != data.Token {
		logger.L.Debug("Invalid callback {id: %s} token: %s", id, data.Token)
		WriteErrorResponse(w, errors.NewInvalidToken())
		return
	}

	// Update callback.
	changes := bson.M{}
	if data.Terminated != nil {
		changes["terminated"] = data.Terminated
	}
	addCallChanges(data.AccountCall, changes, "account_call")
	addCallChanges(data.ClientCall, changes, "client_call")
	if len(changes) != 0 {
		if err = c.callbackSvc.Update(db, cb.Id, changes); err != nil {
			WriteErrorResponse(w, err)
			return
		}
	}

	c.updateCbWrk.UpdateCallback(&cb.Id)

	w.WriteHeader(http.StatusAccepted)
}

func NewCb(
	session service.SessionService,
	account service.AccountService,
	domen service.DomenService,
	request service.RequestService,
	callback service.CallbackService,
	voxImplant service.VoxImplantService,
	updateCbWrk worker.UpdateCallbackWorker,
) *Cb {
	return &Cb{
		sessionSvc:    session,
		accountSvc:    account,
		domenSvc:      domen,
		requestSvc:    request,
		callbackSvc:   callback,
		voxImplantSvc: voxImplant,
		updateCbWrk:   updateCbWrk,
	}
}
