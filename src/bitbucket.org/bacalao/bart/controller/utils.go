package controller

import (
	"bitbucket.org/bacalao/bart/errors"
	"bitbucket.org/bacalao/bart/logger"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

// Max request length in bytes.
const MAX_REQUEST_CONTENT_LENGTH = 1024 * 1024 // 1 Mb

func LogRequest(r *http.Request) {
	logger.L.Debug("%s %s %s", r.Proto, r.Method, r.URL)
}

func ReadRequestBody(r *http.Request) ([]byte, errors.Error) {
	if r.Method == "POST" || r.Method == "PUT" {
		// Get request content length.
		var err error
		var content_length int64
		content_length_str := strings.TrimSpace(r.Header.Get("Content-Length"))
		if content_length_str == "" {
			content_length = int64(MAX_REQUEST_CONTENT_LENGTH)
		} else {
			content_length, err = strconv.ParseInt(content_length_str, 10, 64)
			if err != nil || content_length < 0 {
				return nil, errors.NewLengthRequired()
			}
			if content_length > MAX_REQUEST_CONTENT_LENGTH {
				return nil, errors.NewRequestEntityTooLarge()
			}
		}
		if content_length > 0 {
			body, err := ioutil.ReadAll(io.LimitReader(r.Body, content_length))
			if err != nil && err != io.EOF {
				logger.L.Error("Request body reading error: %s", err)
				return nil, errors.NewInvalidRequest()
			}
			return body, nil
		}
	}
	return nil, nil
}

func WriteErrorResponse(w http.ResponseWriter, httpErr errors.Error) {
	w.Header().Set("Content-Type", "application/json")
	if body, err := json.Marshal(httpErr); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(errors.INTERNAL_SERVER_ERROR))
	} else {
		w.WriteHeader(httpErr.Status())
		w.Write(body)
	}
}

func WriteJSON(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	if body, err := json.Marshal(data); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(errors.INTERNAL_SERVER_ERROR))
	} else {
		w.WriteHeader(http.StatusOK)
		w.Write(body)
	}
}
