package controller

import (
	"bitbucket.org/bacalao/bart/errors"
	"bitbucket.org/bacalao/bart/logger"
	"bitbucket.org/bacalao/bart/service"
	"net/http"
	"net/url"
	"strings"
)

type Options struct {
	// mongodb session service.
	sessionSvc service.SessionService
	// Account service.
	accountSvc service.AccountService
	// Domen service.
	domenSvc service.DomenService
}

// Default handler for OPTIONS requests, it verifies that:
//   * request has Origin header and it's valid;
//   * domen for request Origin header exist;
//   * account for request domen is active.
func (c Options) Options(w http.ResponseWriter, r *http.Request) {
	origin := r.Header.Get("Origin")
	if origin == "" {
		logger.L.Debug("OPTIONS request without origin")
		WriteErrorResponse(w, errors.NewInvalidRequest())
		return
	}

	if strings.ToLower(origin) != "null" {
		pURL, er := url.Parse(origin)
		if er != nil || pURL.Host == "" {
			if er != nil {
				logger.L.Debug("Invalid request Origin %s: %s", origin, er)
			} else {
				logger.L.Debug("Invalid request Origin %s host is not given", origin)
			}
			WriteErrorResponse(w, errors.NewInvalidRequest())
			return
		}

		// Create mongodb session.
		s, err := c.sessionSvc.GetSession()
		if err != nil {
			WriteErrorResponse(w, err)
			return
		}
		defer s.Close()
		db := s.DB("")

		// Get Origin's domen from database and verify that it is allowed.
		logger.L.Info("%s host: %s path: %s", pURL, pURL.Host, pURL.Path)
		domen, err := c.domenSvc.GetByHost(db, pURL.Host)
		if err != nil {
			WriteErrorResponse(w, err)
			return
		}

		// Verify that account is active.
		if _, err := c.accountSvc.GetByRef(db, &domen.AccountRef); err != nil {
			WriteErrorResponse(w, err)
			return
		}
	}

	w.Header().Set("Access-Control-Allow-Origin", origin)
	w.Header().Set("Access-Control-Allow-Methods", "POST, PUT, OPTIONS")

	allowedHeaders := r.Header.Get("Access-Control-Request-Headers")
	if allowedHeaders != "" {
		w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
	}

	w.Header().Set("Access-Control-Allow-Credentials", "false")
	w.WriteHeader(http.StatusOK)
}

func NewOptions(
	session service.SessionService,
	account service.AccountService,
	domen service.DomenService,
) *Options {
	return &Options{
		sessionSvc: session,
		accountSvc: account,
		domenSvc:   domen,
	}
}
