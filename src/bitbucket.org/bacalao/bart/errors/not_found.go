package errors

import "net/http"

func NewNotFound() Error {
	return NewError(http.StatusNotFound, "NotFound", "Not found", nil)
}

func NewUnknownHost() Error {
	return NewError(http.StatusNotFound, "UnknownHost", "Unknown request host",
		nil)
}
