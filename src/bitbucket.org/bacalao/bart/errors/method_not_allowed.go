package errors

import "net/http"

func NewMethodNotAllowed() Error {
	return NewError(http.StatusMethodNotAllowed, "MethodNotAllowed",
		"Method not allowed", nil)
}
