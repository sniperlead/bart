package errors

import "net/http"

func NewInternalServerError() Error {
	return NewError(http.StatusInternalServerError, "InternalServerError",
		"Internal server error", nil)
}
