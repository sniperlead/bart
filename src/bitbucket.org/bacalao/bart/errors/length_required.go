package errors

import "net/http"

func NewLengthRequired() Error {
	return NewError(http.StatusLengthRequired, "LengthRequired",
		"Content-Length header required", nil)
}
