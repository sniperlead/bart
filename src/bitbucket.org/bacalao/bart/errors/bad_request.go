package errors

import "net/http"

func NewAccountDisabled() Error {
	return NewError(http.StatusBadRequest, "AccountDisabled", "Account disabled",
		nil)
}

func NewDomenDisabled() Error {
	return NewError(http.StatusBadRequest, "DomenDisabled", "Domen disabled", nil)
}

func NewInvalidRequest() Error {
	return NewError(http.StatusBadRequest, "InvalidRequest", "Invalid request",
		nil)
}

func NewValidationError(errors map[string]string) Error {
	return NewError(http.StatusBadRequest, "ValidationError", "", errors)
}
