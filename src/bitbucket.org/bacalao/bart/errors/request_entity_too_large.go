package errors

import "net/http"

func NewRequestEntityTooLarge() Error {
	return NewError(http.StatusRequestEntityTooLarge, "RequestEntityTooLarge",
		"Request entity too large", nil)
}
