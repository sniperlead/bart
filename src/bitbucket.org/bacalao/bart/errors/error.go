package errors

import "fmt"

type Error interface {
	error

	Status() int
}

// Implements Error intterface.
type HttpError struct {
	HTTPStatus int               `json:"status"`
	Code       string            `json:"code"`
	Message    string            `json:"error,omitempty"`
	Errors     map[string]string `json:"errors,omitempty"`
}

func (e *HttpError) Status() int {
	return e.HTTPStatus
}

func (e *HttpError) Error() string {
	return fmt.Sprintf("%s (%d) - %s", e.HTTPStatus, e.Code, e.Message)
}

func NewError(status int, code, message string, errors map[string]string) Error {
	return &HttpError{
		HTTPStatus: status,
		Code:       code,
		Message:    message,
		Errors:     errors,
	}
}

const INTERNAL_SERVER_ERROR = `{"status":500,"code":"InternalServerError","error":"Internal server error"}`
