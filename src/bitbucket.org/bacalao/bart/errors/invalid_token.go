package errors

import "net/http"

func NewInvalidToken() Error {
	return NewError(http.StatusUnauthorized, "InvalidToken", "Invalid request token.", nil)
}
