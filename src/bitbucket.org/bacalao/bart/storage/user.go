package storage

import (
	mgo "gopkg.in/mgo.v2"
	bson "gopkg.in/mgo.v2/bson"
	"time"
)

type UserType string

const (
	ROBOT         UserType = "robot"
	ACCOUNT_ADMIN UserType = "account_admin"
	USER          UserType = "user"
)

type User struct {
	Id         bson.ObjectId `bson:"_id,omitempty" json:"id"`
	Verified   bool          `bson:"verified" json:"-"`
	AccountRef mgo.DBRef     `bson:"account_id" json:"-"`
	Type       UserType      `bson:"type" json:"type"`
	// Email should be unique.
	Email     string    `bson:"email" json:"email"`
	Password  string    `bson:"password" json:"-"`
	Status    Status    `bson:"status" json:"status"`
	Deleted   bool      `bson:"deleted" json:"deleted"`
	CreatedAt time.Time `bson:"created_at" json:"created_at"`
	UpdatedAt time.Time `bson:"updated_at" json:"updated_at"`
}

var ROBOT_USER_ID = bson.ObjectIdHex("5570abeac879cbb9f3faef41")
