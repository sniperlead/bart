package storage

import (
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

const DomenCollection = "domens"

type TimeSpentOnPageRule struct {
	Points        int `bson:"point" json:"points"`
	SpentMoreThen int `bson:"spent_more_then" json:"spent_more_then"`
}

type Rules struct {
	TimeSpentOnPage TimeSpentOnPageRule `bson:"time_spent_on_page" json:"time_spent_on_page"`
}

type Domen struct {
	Id         bson.ObjectId `bson:"_id,omitempty" json:"id"`
	AccountRef mgo.DBRef     `bson:"account_id" json:"account_id"`

	Host string `bson:"host" json:"host"`
	// Host should be unique.
	Rules Rules `bson:"rules" json:"rules"`

	Status    Status    `bson:"status" json:"status"`
	Deleted   bool      `bson:"deleted" json:"deleted"`
	CreatedAt time.Time `bson:"created_at" json:"created_at"`
	UpdatedAt time.Time `bson:"updated_at" json:"updated_at"`
}

func (d *Domen) IsActive() bool {
	return d.Status == ACTIVE
}

type DomenDAO interface {
	GetByRef(db *mgo.Database, ref *mgo.DBRef) (*Domen, error)
	GetByHost(db *mgo.Database, host string) (*Domen, error)
}

type DomenDAOImpl struct{}

func (dao *DomenDAOImpl) GetByHost(db *mgo.Database, host string) (*Domen, error) {
	domen := Domen{}
	q := bson.M{"host": host, "deleted": false}
	if err := db.C(DomenCollection).Find(q).One(&domen); err != nil {
		return nil, err
	}
	return &domen, nil
}

func (dao *DomenDAOImpl) GetByRef(db *mgo.Database, ref *mgo.DBRef) (*Domen, error) {
	domen := Domen{}
	q := bson.M{"deleted": false}
	if err := db.FindRef(ref).Select(q).One(&domen); err != nil {
		return nil, err
	}
	return &domen, nil
}

func NewDomenDAO() DomenDAO {
	return &DomenDAOImpl{}
}
