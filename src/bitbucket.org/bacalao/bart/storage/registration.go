package storage

import (
	mgo "gopkg.in/mgo.v2"
	bson "gopkg.in/mgo.v2/bson"
	"time"
)

type Registration struct {
	Id        bson.ObjectId `bson:"_id,omitempty" json:"id"`
	AccountId mgo.DBRef     `bson:"account_id" json:"account_id"`
	UserId    mgo.DBRef     `bson:"user_id" json:"user_id"`
	Deleted   bool          `bson:"deleted" json:"deleted"`
	CreatedAt time.Time     `bson:"created_at" json:"created_at"`
	UpdatedAt time.Time     `bson:"updated_at" json:"updated_at"`
}
