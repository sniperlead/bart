package storage

import (
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"strings"
	"time"
)

const RequestCollection = "requests"

type HTTPProtocol struct {
	Proto string `bson:"proto" json:"proto"`
	Major int    `bson:"major" json:"major"`
	Minor int    `bson:"minor" json:"minor"`
}

type Headers struct {
	UserAgent      string `bson:"user_agent" json:"user_agent"`
	AcceptLanguage string `bson:"accept_language" json:"accept_language"`
}

type Request struct {
	Id bson.ObjectId `bson:"_id,omitempty" json:"id"`

	AccountRef mgo.DBRef `bson:"account_id" json:"-"`
	DomenRef   mgo.DBRef `bson:"domen_id" json:"-"`
	ClientRef  mgo.DBRef `bson:"client_id,omitempty" json:"-"`

	// User request data.
	RemoteAddr string  `bson:"remote_addr" json:"remote_addr"`
	URI        string  `bson:"uri" json:"uri"`
	Host       string  `bson:"host" json:"host"`
	Path       string  `bson:"path" json:"path"`
	Query      string  `bson:"query" json:"query"`
	Referer    string  `bson:"referer" json:"referer"`
	Headers    Headers `bson:"headers" json:"headers"`

	CreatedAt time.Time `bson:"created_at" json:"created_at"`
	UpdatedAt time.Time `bson:"updated_at" json:"updated_at"`
}

type RequestDAO interface {
	GetById(db *mgo.Database, id string) (*Request, error)
	Create(db *mgo.Database, accountId, domenId, clientId bson.ObjectId,
		r *http.Request, uri, host, path, query, referer string) (*Request, error)
}

type RequestDAOImpl struct{}

func (dao *RequestDAOImpl) GetById(db *mgo.Database, id string) (*Request, error) {
	r := Request{}
	err := db.C(RequestCollection).FindId(bson.ObjectIdHex(id)).One(&r)
	if err != nil {
		return nil, err
	}
	return &r, nil
}

func (dao *RequestDAOImpl) Create(
	db *mgo.Database,
	accountId, domenId, clientId bson.ObjectId,
	r *http.Request,
	uri, host, path, query, referer string,
) (*Request, error) {
	now := time.Now().UTC()
	req := &Request{
		Id: bson.NewObjectId(),
		DomenRef: mgo.DBRef{
			Collection: DomenCollection,
			Id:         domenId,
		},
		AccountRef: mgo.DBRef{
			Collection: AccountCollection,
			Id:         accountId,
		},
		ClientRef: mgo.DBRef{
			Collection: ClientCollection,
			Id:         clientId,
		},
		RemoteAddr: r.RemoteAddr,
		URI:        uri,
		Host:       host,
		Path:       path,
		Query:      query,
		Referer:    referer,
		Headers: Headers{
			UserAgent:      strings.TrimSpace(r.Header.Get("User-Agent")),
			AcceptLanguage: strings.TrimSpace(r.Header.Get("Accept-Language")),
		},
		CreatedAt: now,
		UpdatedAt: now,
	}
	if err := db.C(RequestCollection).Insert(req); err != nil {
		return nil, err
	}
	return req, nil
}

func NewRequestDAO() RequestDAO {
	return &RequestDAOImpl{}
}
