package storage

import (
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

const AccountCollection = "accounts"

type Account struct {
	Id bson.ObjectId `bson:"_id,omitempty" json:"id"`
	// Account name should be unique.
	Name        string    `bson:"name" json:"name"`
	Status      Status    `bson:"status" json:"status"`
	Deleted     bool      `bson:"deleted" json:"deleted"`
	PhoneNumber string    `bson:"phone_number" json:"phone_number"`
	CreatedAt   time.Time `bson:"created_at" json:"created_at"`
	UpdatedAt   time.Time `bson:"updated_at" json:"updated_at"`
}

func (a *Account) IsActive() bool {
	return a.Status == ACTIVE
}

var BACALAO_ACCOUNT_ID = bson.ObjectIdHex("556fa7e8c879cbb9f3faef40")

type AccountDAO interface {
	GetByRef(db *mgo.Database, ref *mgo.DBRef) (*Account, error)
}

type AccountDAOImpl struct{}

func (dao AccountDAOImpl) GetByRef(db *mgo.Database, ref *mgo.DBRef) (*Account, error) {
	a := Account{}
	q := bson.M{"deleted": false}
	if err := db.FindRef(ref).Select(q).One(&a); err != nil {
		return nil, err
	}
	return &a, nil
}

func NewAccountDAO() AccountDAO {
	return &AccountDAOImpl{}
}
