package storage

import (
	// "bitbucket.org/bacalao/bart/config"
	// "bitbucket.org/bacalao/bart/logger"
	mgo "gopkg.in/mgo.v2"
)

type Status string

const (
	ACTIVE   Status = "active"
	DISABLED Status = "disabled"
)

var mgoSession *mgo.Session

func GetSession() (*mgo.Session, error) {
	// if mgoSession == nil {
	// 	var err error
	// 	logger.L.Debug("Connecting to MongoDB")
	// 	mgoSession, err = mgo.DialWithInfo(config.C.MongoDB.GetDialInfo())
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// 	logger.L.Info("Successfully connected to MongoDB")
	// }
	return mgoSession.Clone(), nil
}
