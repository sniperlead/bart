package storage

import (
	"bitbucket.org/bacalao/bart/utils"
	"github.com/stretchr/testify/assert"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"strconv"
	"testing"
	"time"
)

func Test_GetById(t *testing.T) {
	assert := assert.New(t)

	env, err := utils.SetupTestEnv()
	assert.Nil(err)
	defer env.Clean()

	dao := NewCallbackDAO()

	_, err = dao.GetById(env.DB, bson.NewObjectId())
	assert.Equal(err, mgo.ErrNotFound)

	id := bson.NewObjectId()
	err = env.DB.C(CallbackCollection).Insert(&Callback{Id: id})
	assert.Nil(err)

	var cb *Callback
	cb, err = dao.GetById(env.DB, id)
	assert.Nil(err)
	assert.Equal(cb.Id, id)
}

func Test_FindUnsynced(t *testing.T) {
	assert := assert.New(t)

	env, err := utils.SetupTestEnv()
	assert.Nil(err)

	dao := NewCallbackDAO()
	r, err := dao.FindUnsynced(env.DB, 10)
	assert.Nil(err)
	assert.Equal(len(r), 0)

	for i := 0; i < 11; i++ {
		cb := &Callback{
			Id:        bson.NewObjectId(),
			CreatedAt: time.Now().UTC().Add(-3 * time.Minute),
			UpdatedAt: time.Now().UTC().Add(-3 * time.Minute),
		}
		if i%2 == 0 {
			cb.SessionId = strconv.Itoa(i)
		}
		assert.Nil(env.DB.C(CallbackCollection).Insert(cb))
	}

	r, err = dao.FindUnsynced(env.DB, 10)
	assert.Nil(err)
	assert.Equal(len(r), 5)
	for _, cb := range r {
		assert.Equal(cb.SessionId, "")
	}
}
