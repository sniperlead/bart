package storage

import (
	"fmt"
	uuid "github.com/nu7hatch/gouuid"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

const CallbackCollection = "callbacks"

type CallError struct {
	Code int
}

type Call struct {
	Id               string     `bson:"id,omitempty"`
	Successful       bool       `bson:"successful,omitempty"`
	StartTime        *time.Time `bson:"start_time,omitempty"`
	PhoneNumber      string     `bson:"phone_number,omitempty"`
	PhoneNumberType  *string    `bson:"phone_number_type,omitempty"`
	TransactionId    *string    `bson:"transaction_id,omitempty"`
	Duration         *int       `bson:"duration,omitempty"`
	RecordURL        *string    `bson:"record_url,omitempty"`
	Cost             *float32   `bson:"cost,omitempty"`
	Voicemail        *bool      `bson:"voicemail,omitempty"`
	VoicemailPattern *string    `bson:"voicemail_pattern,omitempty"`
	Timeout          *bool      `bson:"timeout,omitempty"`
	Error            *CallError `bson:"error,omitempty"`
}

type Callback struct {
	Id bson.ObjectId `bson:"_id,omitempty" json:"id"`

	AccountRef mgo.DBRef `bson:"account_id" json:"-"`
	DomenRef   mgo.DBRef `bson:"domen_id" json:"-"`
	ClientRef  mgo.DBRef `bson:"client_id" json:"-"`
	RequestRef mgo.DBRef `bson:"request_id" json:"-"`

	Terminated bool `bson:"terminated,omitempty"`

	SessionId string `bson:"session_id,omitempty"`

	ExternalId string `bson:"external_id"`

	Token string `bson:"token"`

	Duration int     `bson:"duration,omitempty"`
	Cost     float32 `bson:"cost,omitempty"`

	LogFileUrl *string `bson:"log_file_url,omitempty"`

	AccountCall Call `bson:"account_call" json:"account_call"`
	ClientCall  Call `bson:"client_call" json:"client_call"`

	CreatedAt time.Time `bson:"created_at" json:"created_at"`
	UpdatedAt time.Time `bson:"updated_at" json:"updated_at"`
}

type CallbackDAO interface {
	GetById(db *mgo.Database, id bson.ObjectId) (*Callback, error)
	GetByRequestId(db *mgo.Database, id bson.ObjectId) (*Callback, error)
	Create(db *mgo.Database, r *Request, accountPhoneNum, clientPhoneNum string) (*Callback, error)
	Update(db *mgo.Database, id bson.ObjectId, changes bson.M) error
	FindUnsynced(db *mgo.Database, limit int) ([]Callback, error)
}

type CallbackDAOImpl struct{}

func (dao *CallbackDAOImpl) GetById(db *mgo.Database, id bson.ObjectId) (*Callback, error) {
	cb := Callback{}
	if err := db.C(CallbackCollection).FindId(id).One(&cb); err != nil {
		return nil, err
	}
	return &cb, nil
}

func (dao *CallbackDAOImpl) GetByRequestId(db *mgo.Database, id bson.ObjectId) (*Callback, error) {
	cb := Callback{}
	q := bson.M{
		"request_id": mgo.DBRef{
			Collection: RequestCollection,
			Id:         id,
		},
	}
	if err := db.C(CallbackCollection).Find(q).One(&cb); err != nil {
		return nil, err
	}
	return &cb, nil
}

func (dao *CallbackDAOImpl) Create(db *mgo.Database, r *Request, accountPhoneNum, clientPhoneNum string) (*Callback, error) {
	now := time.Now().UTC()
	var token string
	if t, err := uuid.NewV4(); err != nil {
		return nil, err
	} else {
		token = t.String()
	}
	id := bson.NewObjectId()
	extId := fmt.Sprintf("%s:%s:%s:%s", id.Hex(), token, accountPhoneNum, clientPhoneNum)
	callback := Callback{
		Id:         id,
		AccountRef: r.AccountRef,
		DomenRef:   r.DomenRef,
		ClientRef:  r.ClientRef,
		RequestRef: mgo.DBRef{
			Collection: RequestCollection,
			Id:         r.Id,
		},
		Token:       token,
		ExternalId:  extId,
		AccountCall: Call{PhoneNumber: accountPhoneNum},
		ClientCall:  Call{PhoneNumber: clientPhoneNum},
		CreatedAt:   now,
		UpdatedAt:   now,
	}
	if err := db.C(CallbackCollection).Insert(&callback); err != nil {
		return nil, err
	}
	return &callback, nil
}

func (dao *CallbackDAOImpl) Update(db *mgo.Database, id bson.ObjectId, changes bson.M) error {
	now := time.Now().UTC()
	changes["updated_at"] = now // set updated at
	q := bson.M{"_id": id}
	c := bson.M{"$set": changes}
	return db.C(CallbackCollection).Update(q, c)
}

func (dao *CallbackDAOImpl) FindUnsynced(db *mgo.Database, limit int) ([]Callback, error) {
	r := []Callback{}
	q := bson.M{
		"session_id": bson.M{"$exists": false},
		"$or": []bson.M{
			bson.M{"terminated": true},
			bson.M{
				"updated_at": bson.M{"$lte": time.Now().UTC().Add(-3 * time.Minute)},
			},
		},
	}
	iter := db.C(CallbackCollection).Find(q).Sort("created_at").Limit(limit).Iter()
	defer iter.Close()
	if err := iter.All(&r); err != nil {
		return nil, err
	}
	return r, nil
}

func NewCallbackDAO() CallbackDAO {
	return &CallbackDAOImpl{}
}
