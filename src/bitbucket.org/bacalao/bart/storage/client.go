package storage

import (
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

const ClientCollection = "clients"

type Client struct {
	Id bson.ObjectId `bson:"_id,omitempty" json:"id"`
	// AccountRef mgo.DBRef     `bson:"account_id" json:"account_id"`
	Deleted   bool      `bson:"deleted" json:"deleted"`
	CreatedAt time.Time `bson:"created_at" json:"created_at"`
	UpdatedAt time.Time `bson:"updated_at" json:"updated_at"`
}

type ClientDAO interface {
	GetByRef(db *mgo.Database, ref *mgo.DBRef) (*Client, error)
	Create(db *mgo.Database, id *bson.ObjectId) (*Client, error)
	GetOrCreate(db *mgo.Database, id *bson.ObjectId) (*Client, error)
}

type ClientDAOImpl struct{}

func (d ClientDAOImpl) GetByRef(db *mgo.Database, ref *mgo.DBRef) (*Client, error) {
	c := &Client{}
	if err := db.FindRef(ref).One(c); err != nil {
		return nil, err
	}
	return c, nil
}

func (d ClientDAOImpl) Create(db *mgo.Database, id *bson.ObjectId) (*Client, error) {
	now := time.Now().UTC()
	c := &Client{
		CreatedAt: now,
		UpdatedAt: now,
	}
	if id == nil {
		c.Id = bson.NewObjectId()
	} else {
		c.Id = *id
	}
	if err := db.C(ClientCollection).Insert(c); err != nil {
		return nil, err
	}
	return c, nil
}

func (d ClientDAOImpl) GetOrCreate(db *mgo.Database, id *bson.ObjectId) (*Client, error) {
	if id != nil {
		ref := &mgo.DBRef{
			Collection: ClientCollection,
			Id:         id,
		}
		if c, err := d.GetByRef(db, ref); err == nil {
			return c, nil
		} else if err != mgo.ErrNotFound {
			return nil, err
		}
	}
	return d.Create(db, id)
}

func NewClientDAO() ClientDAO {
	return &ClientDAOImpl{}
}
