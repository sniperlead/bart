package config

import "fmt"

type HTTPConfig struct {
	Host string `json:"host"`
	Port int    `json:"port"`
}

func (c *HTTPConfig) SetDefault() {
	if len(c.Host) == 0 {
		c.Host = "127.0.0.1"
	}
	if c.Port == 0 {
		c.Port = 8080
	}
}

func (c HTTPConfig) GetAddr() string {
	return fmt.Sprintf("%s:%d", c.Host, c.Port)
}
