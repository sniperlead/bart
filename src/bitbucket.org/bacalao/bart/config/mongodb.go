package config

import (
	mgo "gopkg.in/mgo.v2"
	"time"
)

type MongoDBConfig struct {
	Addrs []string `json:"addrs"`

	// Default database name is "bacalao".
	Database string `json:"database"`

	// Default replica set name is "rs0".
	ReplicaSetName string `json:"replicaSet"`

	// Default authorization mechanism is "SCRAM-SHA-1".
	Mechanism string `json:"mechanism"`

	// Default username is "bacalao".
	Username string `json:"username"`

	Password string `json:"password"`
}

// Sets default values for config object.
func (c *MongoDBConfig) SetDefault() {
	if len(c.Database) == 0 {
		c.Database = "bacalao"
	}
	if len(c.ReplicaSetName) == 0 {
		c.ReplicaSetName = "rs0"
	}
	if len(c.Mechanism) == 0 {
		c.Mechanism = "SCRAM-SHA-1"
	}
	if len(c.Username) == 0 {
		c.Username = "bacalao"
	}
}

func (c MongoDBConfig) GetDialInfo() *mgo.DialInfo {
	return &mgo.DialInfo{
		Addrs:          c.Addrs,
		Database:       c.Database,
		ReplicaSetName: c.ReplicaSetName,
		Mechanism:      c.Mechanism,
		Username:       c.Username,
		Password:       c.Password,
		Timeout:        10 * time.Second, // 10 s connection timeout
	}
}
