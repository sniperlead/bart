package config

type Config interface {
	SetDefault()
}

type AppConfig struct {
	Env        string           `json:"env"`
	HTTP       HTTPConfig       `json:"http"`
	MongoDB    MongoDBConfig    `json:"mongodb"`
	VoxImplant VoxImplantConfig `json:"voximplant"`
}

func (c *AppConfig) SetEnv(env string) {
	c.Env = env
}

// Set defaults values for config object.
func (c *AppConfig) SetDefault() {
	c.HTTP.SetDefault()
	c.MongoDB.SetDefault()
}

// var C = AppConfig{}

// func init() {
// 	env := strings.ToLower(strings.TrimSpace(os.Getenv("GO_ENV")))
// 	if len(env) == 0 {
// 		env = "development"
// 	}
// 	path := path.Join("/", "etc", "bacalao", env+".json")
// 	if _, err := os.Stat(path); err != nil && !os.IsNotExist(err) {
// 		panic(err)
// 	}
// 	data, err := ioutil.ReadFile(path)
// 	if err != nil {
// 		panic(err)
// 	}
// 	if err := json.Unmarshal(data, &C); err != nil {
// 		panic(err)
// 	}
// 	C.SetEnv(env)
// 	C.SetDefault()
// }
