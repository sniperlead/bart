package config

type VoxImplantConfig struct {
	// The VoxImplant account ID.
	AccountId string `json:"account_id"`

	// The VoxImplant API key.
	APIKey string `json:"api_key"`

	AppName string `json:"app_name"`

	Username string `json:"username"`

	RuleId string `json:"rule_id"`
}

// Sets default values for config object.
func (c *VoxImplantConfig) SetDefault() {
	if c.Username == "" {
		c.Username = "sniperlead"
	}
	if c.AppName == "" {
		c.AppName = "sniperlead.plusit.voximplant.com"
	}
}
