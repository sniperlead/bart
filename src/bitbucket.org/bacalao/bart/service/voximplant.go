package service

import (
	"bitbucket.org/bacalao/bart/config"
	"bitbucket.org/bacalao/bart/errors"
	"bitbucket.org/bacalao/bart/logger"
	"bitbucket.org/bacalao/bart/voximplant"
)

type VoxImplantService interface {
	StartScenario(data string) (*voximplant.StartScenariosResponse, errors.Error)
	GetCallHistoryByCustomData(data string) (*voximplant.GetCallHistoryResponse, errors.Error)
}

type VoxImplantServiceImpl struct {
	cfg    *config.VoxImplantConfig
	client *voximplant.Client
}

func (s *VoxImplantServiceImpl) StartScenario(data string) (*voximplant.StartScenariosResponse, errors.Error) {
	r, err := s.client.StartScenarios(s.cfg.Username, s.cfg.RuleId, data)
	if err != nil {
		logger.L.Error("VoxImplant error: %s", err)
		return nil, errors.NewInternalServerError()
	}
	return r, nil
}

func (s *VoxImplantServiceImpl) GetCallHistoryByCustomData(data string) (*voximplant.GetCallHistoryResponse, errors.Error) {
	r, err := s.client.GetCallHistoryByCustomData(s.cfg.AppName, data)
	if err != nil {
		logger.L.Error("VoxImplant error: %s", err)
		return nil, errors.NewInternalServerError()
	}
	return r, nil
}

func NewVoxImplantService(cfg *config.VoxImplantConfig, c *voximplant.Client) VoxImplantService {
	return &VoxImplantServiceImpl{cfg: cfg, client: c}
}
