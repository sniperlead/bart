package service

import (
	"bitbucket.org/bacalao/bart/errors"
	"bitbucket.org/bacalao/bart/logger"
	S "bitbucket.org/bacalao/bart/storage"
	mgo "gopkg.in/mgo.v2"
)

type DomenService interface {
	GetByRef(db *mgo.Database, ref *mgo.DBRef) (*S.Domen, errors.Error)
	GetByHost(db *mgo.Database, host string) (*S.Domen, errors.Error)
}

type DomenServiceImpl struct {
	dao S.DomenDAO
}

func (s *DomenServiceImpl) GetByHost(db *mgo.Database, host string) (*S.Domen, errors.Error) {
	if domen, err := s.dao.GetByHost(db, host); err == nil {
		if domen.IsActive() {
			return domen, nil
		}
		logger.L.Debug("Domen {id: %s} is disabled", domen.Id)
		return nil, errors.NewDomenDisabled()
	} else if err == mgo.ErrNotFound {
		logger.L.Debug("Domen {host: %s} not found", host)
		return nil, errors.NewNotFound()
	} else {
		logger.L.Error("Cannot get domen by host %s: %s", host, err)
		return nil, errors.NewInternalServerError()
	}
}

func (s *DomenServiceImpl) GetByRef(db *mgo.Database, ref *mgo.DBRef) (*S.Domen, errors.Error) {
	if domen, err := s.dao.GetByRef(db, ref); err == nil {
		if domen.IsActive() {
			return domen, nil
		}
		return nil, errors.NewDomenDisabled()
	} else if err == mgo.ErrNotFound {
		logger.L.Debug("Domen {id: %s} not found", ref.Id)
		return nil, errors.NewNotFound()
	} else {
		logger.L.Error("Cannot get domen {id: %s}: %s", ref.Id, err)
		return nil, errors.NewInternalServerError()
	}
}

func NewDomenService(dao S.DomenDAO) DomenService {
	return &DomenServiceImpl{dao: dao}
}
