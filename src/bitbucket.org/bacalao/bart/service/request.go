package service

import (
	"bitbucket.org/bacalao/bart/errors"
	"bitbucket.org/bacalao/bart/logger"
	S "bitbucket.org/bacalao/bart/storage"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"net/http"
)

type RequestService interface {
	GetById(db *mgo.Database, id string) (*S.Request, errors.Error)

	Create(
		db *mgo.Database,
		accountId, domenId, clientId bson.ObjectId,
		r *http.Request,
		uri, host, path, query, referer string,
	) (*S.Request, errors.Error)
}

type RequestServiceImpl struct {
	dao S.RequestDAO
}

func (s *RequestServiceImpl) GetById(db *mgo.Database, id string) (*S.Request, errors.Error) {
	if r, err := s.dao.GetById(db, id); err == nil {
		return r, nil
	} else if err == mgo.ErrNotFound {
		logger.L.Debug("Request {id: %s} not found", id)
		return nil, errors.NewNotFound()
	} else {
		logger.L.Error("Cannot get request {id: %s}: %s", id, err)
		return nil, errors.NewInternalServerError()
	}
}

func (s *RequestServiceImpl) Create(
	db *mgo.Database,
	accountId, domenId, clientId bson.ObjectId,
	r *http.Request,
	uri, host, path, query, referer string,
) (*S.Request, errors.Error) {
	req, err := s.dao.Create(db, accountId, domenId, clientId, r, uri, host,
		path, query, referer)
	if err != nil {
		logger.L.Error("Cannot create request: %s", err)
		return nil, errors.NewInternalServerError()
	}
	return req, nil
}

func NewRequestService(dao S.RequestDAO) RequestService {
	return &RequestServiceImpl{dao: dao}
}
