package service

import (
	"bitbucket.org/bacalao/bart/config"
	"bitbucket.org/bacalao/bart/errors"
	"bitbucket.org/bacalao/bart/logger"
	mgo "gopkg.in/mgo.v2"
)

type SessionService interface {
	GetSession() (*mgo.Session, errors.Error)
}

type SesssionServiceImpl struct {
	cfg     *config.MongoDBConfig
	session *mgo.Session
}

func (s *SesssionServiceImpl) GetSession() (*mgo.Session, errors.Error) {
	if s.session == nil {
		var err error
		logger.L.Debug("Connecting to MongoDB")
		if s.session, err = mgo.DialWithInfo(s.cfg.GetDialInfo()); err != nil {
			logger.L.Error("MongoDB connection error: %s", err)
			return nil, errors.NewInternalServerError()
		}
		logger.L.Debug("Successfully connected to MongoDB")
	}
	return s.session.Clone(), nil
}

func NewSessionService(cfg *config.MongoDBConfig) SessionService {
	return &SesssionServiceImpl{cfg: cfg}
}
