package service

import (
	"bitbucket.org/bacalao/bart/errors"
	"bitbucket.org/bacalao/bart/logger"
	S "bitbucket.org/bacalao/bart/storage"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type ClientService interface {
	GetOrCreate(db *mgo.Database, id *bson.ObjectId) (*S.Client, errors.Error)
}

type ClientServiceImpl struct {
	dao S.ClientDAO
}

func (s *ClientServiceImpl) GetOrCreate(db *mgo.Database, id *bson.ObjectId) (*S.Client, errors.Error) {
	if c, err := s.dao.GetOrCreate(db, id); err == nil {
		return c, nil
	} else {
		logger.L.Error("Can't create or get client {id: %s}: %s", id, err)
		return nil, errors.NewInternalServerError()
	}
}

func NewClientService(dao S.ClientDAO) ClientService {
	return &ClientServiceImpl{dao: dao}
}
