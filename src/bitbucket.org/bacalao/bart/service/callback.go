package service

import (
	"bitbucket.org/bacalao/bart/errors"
	log "bitbucket.org/bacalao/bart/logger"
	S "bitbucket.org/bacalao/bart/storage"
	mgo "gopkg.in/mgo.v2"
	bson "gopkg.in/mgo.v2/bson"
)

type CallbackService interface {
	GetById(db *mgo.Database, id bson.ObjectId) (*S.Callback, errors.Error)
	GetByRequestId(db *mgo.Database, id bson.ObjectId) (*S.Callback, errors.Error)
	Create(db *mgo.Database, r *S.Request, accountPhoneNum, clientPhoneNum string) (*S.Callback, errors.Error)
	Update(db *mgo.Database, id bson.ObjectId, changes bson.M) errors.Error
	FindUnsynced(db *mgo.Database, limit int) ([]S.Callback, errors.Error)
}

type CallbackServiceImpl struct {
	dao S.CallbackDAO
}

func (s *CallbackServiceImpl) GetById(db *mgo.Database, id bson.ObjectId) (*S.Callback, errors.Error) {
	if callback, err := s.dao.GetById(db, id); err == nil {
		return callback, nil
	} else if err == mgo.ErrNotFound {
		log.L.Debug("Callback {id: %s} not found", id)
		return nil, errors.NewNotFound()
	} else {
		log.L.Error("Cannot get callback {id: %s}: %s", id, err)
		return nil, errors.NewInternalServerError()
	}
}

func (s *CallbackServiceImpl) GetByRequestId(db *mgo.Database, id bson.ObjectId) (*S.Callback, errors.Error) {
	if callback, err := s.dao.GetByRequestId(db, id); err == nil {
		return callback, nil
	} else if err == mgo.ErrNotFound {
		log.L.Debug("Callback {id: %s} not found", id)
		return nil, errors.NewNotFound()
	} else {
		log.L.Error("Cannot get callback {id: %s}: %s", id, err)
		return nil, errors.NewInternalServerError()
	}
}

func (s *CallbackServiceImpl) Create(db *mgo.Database, r *S.Request, accountPhoneNum, clientPhoneNum string) (*S.Callback, errors.Error) {
	cb, err := s.dao.Create(db, r, accountPhoneNum, clientPhoneNum)
	if err != nil {
		tmpl := "Cannot create callback {request_id: %s, account_phone_number: %s, client_phone_number: %s}: %s"
		log.L.Error(tmpl, r.Id.Hex(), accountPhoneNum, clientPhoneNum, err)
		return nil, errors.NewInternalServerError()
	}
	return cb, nil
}

func (s *CallbackServiceImpl) Update(db *mgo.Database, id bson.ObjectId, changes bson.M) errors.Error {
	if err := s.dao.Update(db, id, changes); err != nil {
		log.L.Error("Cannot update callback entity {id: %s}: %s", id.Hex(), err)
		return errors.NewInternalServerError()
	}
	return nil
}

func (s *CallbackServiceImpl) FindUnsynced(db *mgo.Database, limit int) ([]S.Callback, errors.Error) {
	if r, err := s.dao.FindUnsynced(db, limit); err == nil {
		return r, nil
	} else {
		log.L.Error("Cannot get a list of unsynced callbacks: %s", err)
		return nil, errors.NewInternalServerError()
	}
}

func NewCallbackService(dao S.CallbackDAO) CallbackService {
	return &CallbackServiceImpl{dao: dao}
}
