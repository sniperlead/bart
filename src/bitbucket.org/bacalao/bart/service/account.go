package service

import (
	"bitbucket.org/bacalao/bart/errors"
	"bitbucket.org/bacalao/bart/logger"
	S "bitbucket.org/bacalao/bart/storage"
	mgo "gopkg.in/mgo.v2"
)

type AccountService interface {
	GetByRef(db *mgo.Database, ref *mgo.DBRef) (*S.Account, errors.Error)
}

type AccountServiceImpl struct {
	dao S.AccountDAO
}

func (s *AccountServiceImpl) GetByRef(db *mgo.Database, ref *mgo.DBRef) (*S.Account, errors.Error) {
	if account, err := s.dao.GetByRef(db, ref); err == nil {
		if account.IsActive() {
			return account, nil
		}
		logger.L.Debug("Account {id: %s} is disabled", account.Id)
		return nil, errors.NewAccountDisabled()
	} else if err == mgo.ErrNotFound {
		logger.L.Debug("Account {id: %s} not found", ref.Id)
		return nil, errors.NewNotFound()
	} else {
		logger.L.Error("Cannot get account {id: %s}: %s", ref.Id, err)
		return nil, errors.NewInternalServerError()
	}
}

func NewAccountService(dao S.AccountDAO) AccountService {
	return &AccountServiceImpl{dao: dao}
}
