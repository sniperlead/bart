ALL: format build

clean:
	-rm -f build/server

run: build
	./build/bart

build: clean
	go build -o build/server main.go

format:
	gofmt -w=true src/bitbucket.org/*/*/*.go src/bitbucket.org/*/*/*/*.go

test:
	go test bitbucket.org/bacalao/bart/utils
	go test bitbucket.org/bacalao/bart/storage
