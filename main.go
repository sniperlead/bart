package main

import (
	"os"
	"bitbucket.org/bacalao/bart"
	cfg "bitbucket.org/bacalao/bart/config"
	"encoding/json"
	"strings"
	"path"
	"io/ioutil"
)

func main() {
	env := strings.ToLower(strings.TrimSpace(os.Getenv("GO_ENV")))
	if len(env) == 0 {
		env = "development"
	}
	path := path.Join("/", "etc", "bacalao", env+".json")
	if _, err := os.Stat(path); err != nil && !os.IsNotExist(err) {
		panic(err)
	}
	data, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	c := cfg.AppConfig{}
	if err := json.Unmarshal(data, &c); err != nil {
		panic(err)
	}
	c.SetEnv(env)
	c.SetDefault()
	app := bart.NewApp(&c)
	os.Exit(app.Start())
	// client := voximplant.New("79697", "24101d43-0461-4f0c-8441-e55262a52e45")
	// d := url.Values{}
	// d.Add("from_date", "2015-07-15 00:00:00")
	// d.Add("to_date", "2015-07-24 00:00:00")
	// d.Add("application_name", "sniperlead.plusit.voximplant.com")
	// d.Add("with_calls", "true")
	// d.Add("call_session_history_custom_data", "55aec69a5d24f68bdc000018:7dbd4bbb-a4b6-4e60-59a1-ab765dde22a1:79219315811:79531431135")
	// resp, err := client.Request("GetCallHistory", d)
	// fmt.Println(resp.Result)
	// fmt.Println(string(resp))
	// fmt.Println(err)
}
